const ENTER = 13;
const ESCAPE = 27;
const TAB = 9;

const PHONE_RE = /^\+?\s*[\d\s()]+$/;  // string already trimmed

var FIELDS = [{
        name: 'phone',
        placeholder: '+38xxxxxx',
    }, {
        name: 'title',
        placeholder: 'безимянная компания',
    }]
var FIELDS2 = FIELDS.map((x) => x.name);
FIELDS2 = FIELDS2.concat(FIELDS2);


function glyph(name) {
    return <span className={'glyphicon glyphicon-' + name} />
}


var AutoFocusInput = React.createClass({
    render() {
        return <input value={ this.props.value }
            placeholder={ this.props.placeholder }
            onChange={ this.props.onChange }
            onKeyUp={ this.keyup }
            onKeyDown={ this.keydown }
            />
    },
    componentDidMount() {
        this.getDOMNode().focus()
        this.getDOMNode().setSelectionRange(
            this.props.value.length, this.props.value.length)
    },
    keydown(ev) {
        if(ev.which == TAB) {
            ev.preventDefault();
        }
    },
    keyup(ev) {
        var fun;
        if(ev.which == ENTER) {
            fun = this.props.onEnter;
        } else if(ev.which == ESCAPE) {
            fun = this.props.onEscape;
        } else if(ev.which == TAB) {
            fun = this.props.onTab;
        }
        if(fun) {
            fun(ev)
        }
    },
})


var Editable = React.createClass({
    getInitialState() {
        return {}
    },
    render() {
        var edit = this.props.onEdit === undefined ?
            this.state.edit : this.props.edit;
        return <span
                onMouseOver={ () => this.setState({hover: true }) }
                onMouseOut={ () => this.setState({hover: false}) }
                >
                { edit && <span>
                    <AutoFocusInput
                        value={this.props.value}
                        placeholder={ this.props.placeholder }
                        onChange={this.props.onChange}
                        onEnter={ this.enter }
                        onEscape={ this.escape }
                        onTab={ this.props.onTab }
                        />
                    <button className="btn btn-xs btn-default"
                            onClick={ this.enter }>
                        { glyph('ok') }</button>
                    <button className="btn btn-xs btn-default"
                            onClick={ this.escape }>
                        { glyph('remove') }</button>
                </span> }
                { !edit && <span
                    className={ this.props.value ? '' : 'text-muted' }>
                    { this.props.value || this.props.placeholder } {}
                    <button className={
                        'btn btn-default btn-xs ' +
                        (this.state.hover ? '' : 'invisible')}
                        onClick={ this.edit_start }>
                        { glyph('edit') }
                    </button>
                </span> }
        </span>
    },
    edit_start() {
        if(this.props.onEdit) {
            this.props.onEdit();
        } else {
            this.setState({'edit': true});
        }
    },
    enter() {
        if(this.props.onEdit) {
            this.props.onEnter && this.props.onEnter();
        } else {
            this.setState({'edit': false});
        }
    },
    escape() {
        if(this.props.onEdit) {
            this.props.onEscape && this.props.onEscape();
        } else {
            this.setState({'edit': false});
        }
    },
})

var NewIssue = React.createClass({
    getInitialState() {
        setTimeout(() => {
            fetch('/issue/get').then((resp) => {
                resp.json().then((data) => {
                    this.setState({
                        'title': data.title,
                        'companies': Immutable.fromJS(data.companies),
                        })
                });
            })
        }, 100)
        return {
            'title': 'Новая задача',
            'companies': Immutable.List(),
            'edit_id': null,
            'edit_field': null,
            }
    },
    render() {
        return <div>
            <h1><Editable
                value={ this.state.title }
                onChange={ this.update_title } /></h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>Номер</th>
                        <th>Название</th>
                    </tr>
                </thead>
                <tbody>
                    { this.state.companies.map(this.render_row).toJS() }
                </tbody>
           </table>
           <input ref="new_company" className="form-control"
            value={ this.state.new_text }
            onChange={ this.company_text_change }
            placeholder="Начните набирать номер телефона или название" />
        </div>;
    },
    row_change(idx, field, ev) {
        this.setState({
            'companies': this.state.companies.setIn(
                [idx, field], ev.target.value),
            })
        var obj = {
                'id': this.state.companies.get(idx).get('id'),
                }
        obj[field] = ev.target.value
        fetch('/issue/modify_company', {
            method: 'post',
            body: JSON.stringify(obj),
            })
    },
    set_next_field() {
        var field_name = FIELDS2[FIELDS2.indexOf(this.state.edit_field)+1];
        this.setState({'edit_field': field_name});
    },
    set_next_empty(idx) {
        for(var i = FIELDS2.indexOf(this.state.edit_field)+1,
                il = FIELDS2.length; i < il; ++i) {
            if(this.state.companies.get(idx).get(FIELDS2[i]) == '') {
                this.setState({'edit_field': FIELDS2[i]});
                return;
            }

        }
        this.setState({'edit_id': null, 'edit_field': null});
        this.refs.new_company.getDOMNode().focus();
    },
    render_field(row, idx, field) {
        return <td key={ field.name }><Editable
            edit={ this.state.edit_id == row.get('id') &&
                   this.state.edit_field == field.name }
            onEdit={ () => this.setState({'edit_id': row.get('id'),
                                          'edit_field': field.name}) }
            onEnter={ this.set_next_empty.bind(this, idx) }
            onTab={ this.set_next_field }
            onEscape={ () => this.setState({'edit_id': null,
                                            'edit_field': null}) }
            onChange={ this.row_change.bind(this, idx, field.name) }
            value={ row.get(field.name) }
            placeholder={ field.placeholder } /></td>
    },
    render_row(row, idx) {
        return <tr key={ row.get('id') }>
            { FIELDS.map(this.render_field.bind(this, row, idx)) }
        </tr>
    },
    update_title(ev) {
        this.setState({title: ev.target.value})
        fetch('/issue/set_title', {
            method: 'post',
            body: JSON.stringify({'title': ev.target.value}),
            })
    },
    company_text_change(ev) {
        var val = ev.target.value.trim();
        this.setState({'new_text':  val})
        if(val.length > 3) {
            var rec = {
                id: this.state.companies.count() + 1,
                phone: '',
                title: '',
            };
            var fld;
            if(PHONE_RE.exec(val)) {
                rec.phone = val;
                fld = 'phone';
            } else {
                rec.title = val;
                fld = 'title';
            }
            this.setState({
                'companies': this.state.companies.push(Immutable.Map(rec)),
                'new_text': '',
                'edit_id': rec.id,
                'edit_field': fld,
                })
            fetch('/issue/add_company', {
                method: 'post',
                body: JSON.stringify(rec),
                })
        }
    },
})


