//$(document).ready(function(){
//    var phoneNumberRegex1 = /[+38]{0,1}[\ ]{0,1}[(]{0,1}(\d{3})[)]{0,1}\ (\d{3})[-]{0,1}[\ ]{0,1}(\d{2})-{0,1}[\ ]{0,1}(\d{2})/g;
//
//
//    function addButtons(){
//        $('.d-call').remove();
//        var body = document.body.innerHTML;
//        body = body.replace(phoneNumberRegex1, '$&<button data-number="$&" class="d-call">&#9990;</button>');
//        document.body.innerHTML = body;
//
//        $('.d-call').click(function(e){
//            e.preventDefault();
//            var num = $(this).attr('data-number').replace(/-/g, '').replace(/\ /g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\+/g, '');
//            var data = {number: num, site: location.href}
//            var url = 'http://api.d-call.ngrok.com/call'
//            $.post(url, JSON.stringify(data), function(res){
//             //do something
//            });
//            console.log(chrome.tabs);
//        });
//    }
//    addButtons();
//})
function dCallGetXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}
function $(selector, callback, params){
    var objects;
    if(this.document)objects=document.querySelectorAll(selector);
    else objects=this.querySelectorAll(selector);
    if(objects.length>0){
        if(callback)for(i=0; i<objects.length; i++){
            if(params)callback(objects.item(i), params);
            else callback(objects.item(i));
        }
        if(objects.length==1)return objects[0]; else return objects;
    } else return false;
}
Element.prototype.remove=function(){this.parentNode.removeChild(this);}
Element.prototype.attr=function(attr, val){
    if(!val)return this.getAttribute(attr);
    else if(val=='')this.removeAttribute(attr);
    else this.setAttribute(attr, val);
}
NodeList.prototype.remove=function(){
    var i;
    for(i=0; i<this.length; i++)this.item(i).remove();
}
function ajax(url, params, callback){
    var xhr = dCallGetXmlHttp()
    if(!params)params='{}';
    xhr.open('post', url, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    xhr.onload=function(r){
        if(callback)callback()
    }
}
function dCallAddButtons(){
    var phoneNumberRegex1 = /[+38]{0,1}[\ ]{0,1}[-]{0,1}[\ ]{0,1}[(]{0,1}0(\d{2})[)]{0,1}[\ ]{0,1}[-]{0,1}[\ ]{0,1}(\d{3})[\ ]{0,1}[-]{0,1}[\ ]{0,1}(\d{2})[\ ]{0,1}[-]{0,1}[\ ]{0,1}(\d{2})/g;
    var buttonList = $('.d-call');
    if(buttonList)buttonList.remove();
    var allTextNodes = document.createTreeWalker(document.body,NodeFilter.SHOW_TEXT);
    while(allTextNodes.nextNode()){
        tmpnode = allTextNodes.currentNode;
        var b = document.createElement('button');
        b.innerHTML = '&#9990;';
        b.className = 'd-call';
        b.setAttribute('data-number', tmpnode.nodeValue);
        b.onclick=function(){
            var num = this.attr('data-number').replace(/-/g, '').replace(/\ /g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\+/g, '');
            var data = {number: num, site: location.href}
            var url = 'http://api.d-call.ngrok.com/site_call/';
            var params = JSON.stringify(data)
//            var str = []
//            for(var p in data)
//                if (data.hasOwnProperty(p)) {
//                    str.push(p + "=" + encodeURIComponent(data[p]));
//                }
//            str.join("&");
            params = 'number=' + encodeURIComponent(num) + '&site=' + encodeURIComponent(location.href);
            ajax(url, params);
        }
        if(tmpnode.nodeValue.match(phoneNumberRegex1)){
            tmpnode.parentElement.appendChild(b);
        }
    }
}
function dCallInit(){
    dCallAddButtons();
    dCallInited = true;
}
var dCallInited = false;

document.body.onclick = function(){
    setTimeout(function(){
        dCallAddButtons();
    }, 1000);
}
setTimeout(function(){
    if(!dCallInited)dCallInit();
}, 1000)