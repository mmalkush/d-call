import tornado.web
import math
import urllib.parse as urlparse
import momoko
import psycopg2


class Profile(tornado.web.UIModule):
    def render(self, user, login_url):
        return self.render_string("modules/profile.html", user=user, login_url=login_url)


def update_querystring(url, **kwargs):
    base_url = urlparse.urlsplit(url)
    query_args = urlparse.parse_qs(base_url.query)
    query_args.update(kwargs)
    for arg_name, arg_value in kwargs.items():
        if arg_value is None:
            if arg_name in query_args:
                del query_args[arg_name]

    query_string = urlparse.urlencode(query_args, True)
    return urlparse.urlunsplit((base_url.scheme, base_url.netloc, base_url.path.replace('/ajax/', '/'), query_string, base_url.fragment))


class Paginator(tornado.web.UIModule):
    """Pagination links display."""

    def render(self, page, page_size, results_count, vertical=False):
        pages = int(math.ceil(results_count / page_size)) if results_count else 0

        def get_page_url(page):
            # don't allow ?page=1
            if page <= 1:
                page = None
            return update_querystring(self.request.uri, page=page)

        next_page = page + 1 if page < pages else None
        prev_page = page - 1 if page > 1 else None

        if vertical:
            nav_class = 'vertical'
        else:
            nav_class = 'horizontal'

        return self.render_string('modules/pagination.html', page=page, pages=pages, next=next_page, previous=prev_page, get_page_url=get_page_url, nav_class=nav_class)


class Form(tornado.web.UIModule):
    def render(self, form_data):
        if form_data:
            template = 'modules/forms/%s.html' % (form_data["type"],)
            return self.render_string(template, inputs=form_data["inputs"], message=form_data["message"], form_class=form_data["type"], action=form_data["action"], title=form_data["title"])
        else:
            return ''


class LastPubTitles(tornado.web.UIModule):
    query = "SELECT title_%s as title, slug FROM %s WHERE hidden = False ORDER BY created LIMIT %s OFFSET %s"

    @tornado.gen.coroutine
    def render(self, lang, table, link, page=1, limit=10):
        offset = (page - 1) * limit
        limit += 1
        query = self.query % (lang, table, limit, offset)
        cursor = yield momoko.Op(self.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
        rows = cursor.fetchall()
        if len(rows) > 10:
            next_page = page + 1
            del rows[-1]
        else:
            next_page = None

        return self.render_string('modules/last_pub_titles.html', rows=rows, next_page=next_page, link=link)


class SubMenu(tornado.web.UIModule):
    def render(self, lang, link, rows):
        return self.render_string('modules/submenu.html', rows=rows, lang=lang, link=link)