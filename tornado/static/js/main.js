function ajax(url, params, callback, form, method){
    beforeLoad();
    if(callback)var node=this;
    if(!method){
        if(form)var method=this.method; else var method='get'
    }
    var xhr = getXmlHttp()
    if(url.indexOf(location.host) > -1)url=url.replace(location.host, location.host+'/ajax');
    if(url.indexOf('ajax') <= -1)url = '/ajax'+url;
    //if(url[url.length-1] != '/')url+='/';
    if(method=='get'){
        if(params && params != null){
            url=strstr(url, '?', true);
            url=url+'?'+params
        }
        xhr.open(method, url, true);
        xhr.send();
    } else {
        if(!params)params='';
        xhr.open(method, url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
    }
    xhr.onload=function(){
        if(callback){
            callback(node, xhr.response);
        } else {
            defaultCallback(xhr.response, form);
        }
        afterLoad();
    }
}
Element.prototype.ajax=ajax;

function ajaxURL(url, addPrefix){
    if(url=='' || url==null)url=location.href;
    if (addPrefix)url=url.replace(location.host, location.host+'/ajax');
    return url;
}

function sendForm(form, callback){
    beforeLoad();
    if(callback)var node=form;
    var formData = new FormData(form);
    var method=form.method;
    var action=ajaxURL(form.attr('action'), true)
    var xhr = new XMLHttpRequest();
    xhr.open(method, action, true);
    xhr.onload = function(e) {
        if(callback){
            callback(node, xhr.response);
        } else defaultCallback(xhr.response, form);
        afterLoad();
    }
    xhr.send(formData);
    return false;
}

function submit(form, callback){
    if(!callback)callback=null;

    for(var i in tinyEditors){
        tinyEditors[i].post()
    }

    if(form.enctype=='multipart/form-data'){
        sendForm(form, callback);
    } else {
        var params=serialize(form);
        form.ajax(ajaxURL(form.attr('action')), params, callback, form);
    }
}

function initSubmit(form){
    form.onsubmit=function(){
        if(!this.hasClass('_reload')){
            submit(this);
            return false;
        }
    }
}

function initLink(link){
    link.onclick=function(){
        if(this.hasClass('_reload'))return;
        if(this.hasClass('_close')){
            //TODO: if it is set data-level attribute set parent by attribute value   for(i<data-level)box = box.parentNode
            var box = this.parentNode.parentNode.parentNode.parentNode;
            box.innerHTML = '';
            box.removeClass('_loaded');
            return;
        }
        if(this.hasClass('_oauth')){
            window.open(this.href, 'oauth', "height=480,width=640,menubar=no,location=no,resizable=no,scrollbars=no,status=no");
            return false;
        }
        ajax(link.href);
        return false;
    }
}

function initTab(li){
    li.onclick=function(){
        var i = this.index();
        this.parentNode.$('li').removeClass('active');
        var box = this.parentNode.nextElementSibling;
        box.$('div').removeClass('active');
        box.children[i].addClass('active');
        this.parentNode.children[i].addClass('active');
    }
}

function initRow(row){
    row.ondblclick=function(){
        ajax(row.attr('data-url'));
        return false;
    }
}

function initDropDown(title, params){
    title.onclick=function(){
        if(title.attr('data-open')=='true'){
            title.attr('data-open', 'false')
            if(params.parentOpenClass)title.parentNode.removeClass(params.parentOpenClass)
        } else {
            title.attr('data-open', 'true')
            if(params.parentOpenClass)title.parentNode.addClass(params.parentOpenClass)
        }
        if(title.href='#')return false
    }
}

var tinyEditors = []
function initEditor(textarea){
    textarea.attr('data-eid', tinyEditors.length)
    var ed = new TINY.editor.edit('editor',{
        textarea:textarea,
        width:800,
        height:400,
        cssclass:'te',
        controlclass:'tecontrol',
        rowclass:'teheader',
        dividerclass:'tedivider',
        controls:['bold','italic','underline','strikethrough','|','subscript','superscript','|',
                  'orderedlist','unorderedlist','|','outdent','indent','|','leftalign',
                  'centeralign','rightalign','blockjustify','|','unformat','|','undo','redo','n',
                  'style','|','image','hr','link','unlink'],
        footer:true,
        fonts:['Verdana','Arial','Georgia','Trebuchet MS'],
        xhtml:true,
        cssfile:'style.css',
        bodyid:'editor',
        footerclass:'tefooter',
        toggle:{text:'source',activetext:'wysiwyg',cssclass:'toggle'},
        resize:{cssclass:'resize'}
    });
    tinyEditors.push(ed)
}

function notice(text, type, form){
    if(form){
        var box = form.$('.form-message')
    } else {
        var box = $('#notice')
    }
    box.innerHTML='<span class="'+type+'">'+text+'</span>';
    box.addClass('show')
    setTimeout(function(){box.removeClass('show');}, 4000);
    setTimeout(function(){box.innerHTML='';}, 5000);
}

function fieldError(field, text){
    field.addClass('error');
    var label = document.createElement('label');
    label.innerHTML = text;
    field.insertElementAfter(label);
}

function defaultCallback(response, form){
    response=JSON.parse(response);
    if(response.message>'')notice(response.message, response.status, form);
    if(form && response.status=='error'){
        if(response.data.formErrors){
            for(var name in response.data.formErrors)fieldError(form.elements[name], response.data.formErrors[name])
        }
    }
    if(response.data.formValues){
        var formValues = response.data.formValues
        for(var name in formValues){
            form.elements[name].value = formValues[name]
            if(form.elements[name].hasClass('_WYSIWYG')){
                var textarea = form.elements[name]
                var editor = tinyEditors[parseInt(textarea.attr('data-eid'))]
                editor.e.body.innerHTML = textarea.value
            }
        }
    }
    if(response.data.action)form.action = response.data.action;
    if(response.data.resetForm)form.reset();
    if(response.data.html && response.data.element)responseHTML(response.data.html, response.data.element, response.data.htmlInsert)
    if(response.data.html2 && response.data.element2)responseHTML(response.data.html2, response.data.element2, response.data.htmlInsert2)
    if(response.code)eval(response.code);
}

function responseHTML(data, el, htmlInsert){
    var box = $(el);
    if(htmlInsert){
        if(htmlInsert=='before'){
            box.innerHTML=data+box.innerHTML;
        } else if(htmlInsert=='after'){
            box.innerHTML=box.innerHTML+data;
        }
    } else box.innerHTML=data;
    initAjax(box);
    box.addClass('_loaded')
    setTimeout(function(){box.removeClass('_loaded');}, 2000);
}

function ac(inp, selectCallback){
    function acSelect(li){
        li.onclick=function(){
            inp.value=this.attr('data-name');
            inp.previousSibling.value=this.attr('data-id');
            inp.nextElementSibling.hide();
            //inp.set.value=this.attr('data-setname');
            //inp.set_int.value=this.attr('data-setid');
            if(inp.child)inp.child.focus();
            if(selectCallback)selectCallback(inp);
        }
    }
    function autocomplete(event){
        var keyCode = event.keyCode ? event.keyCode : event.which;
        if(this.value.length>2 && keyCode!=13 && keyCode!=38 && keyCode!=40){
            var url = this.attr('data-href');
            var p = 'type='+this.attr('data-type')+'&value='+this.value;
            //if(!this.relation)relation(this);
            //if(this.parent)p+='&parent_value='+this.parent_int.value;
            //inp.set.value='';
            //inp.set_int.value=0;

            this.ajax(url, p, function (node, response){
                //випадаючий список
                response=JSON.parse(response);
                var l = response.data.html
                var list=node.nextElementSibling;
                if(l > ''){
                    list.innerHTML = l;
                    list.show();
                    list.$('li', acSelect);
                } else {
                    list.hide();
                }

            });
        }
    }
    function handleKey(event, inp) {
        var keyCode = event.keyCode ? event.keyCode : event.which;
        if(keyCode==13 || keyCode==38 || keyCode==40){
            var active=inp.nextElementSibling.$('li.active');
            if(active){
                switch(keyCode){
                    case 13:
                        active.click();
                    break;
                    case 40:
                        newActive=active.nextElementSibling;
                        if(newActive){
                            active.removeClass('active');
                            newActive.addClass('active');
                        }
                    break;
                    case 38:
                        var newActive=active.previousSibling;
                        if(newActive){
                            active.removeClass('active');
                            newActive.addClass('active');
                        }
                    break;
                }
            }
            event.preventDefault();
            event.stopPropagation();
            return false;
        } else return true;
    }
    function relation(inp){
        inp.relation=true;
        /*inp.parent=$('#'+inp.attr('data-parent')+'_ac');
        inp.parent_int=$('#'+inp.attr('data-parent')+'_id');
        inp.child=$('#'+inp.attr('data-child')+'_ac');
        inp.child_int=$('#'+inp.attr('data-child')+'_id');*/
        //inp.set_int=inp.previousSibling
    }
    inp.onkeydown=function(event){ return handleKey(event, this);}
    inp.onkeyup=autocomplete.debounce(600);
    inp.onblur=function(){setTimeout(function(){if(inp.nextElementSibling)inp.nextElementSibling.hide();}, 100);}
}

function tagAC(inp){
    var addTag = function(inp){
        var v = inp.value;
        var k = inp.previousSibling.value;
        var ul = inp.parentNode.parentNode.previousSibling;
        var li = '<li><input type="hidden" name="' + inp.previousSibling.name + '[]" value="' + k + '" />' + v + '<i class="_remove_parent fa fa-times"></i></li>';
        ul.innerHTML += li;
        initAjax(ul);
        inp.value = '';
    }
    ac(inp, addTag)
}

function lookup(inp){
    function trSelect(li){
        li.onclick=function(){
            inp.value=this.children[0].innerHTML;
            ajax(this.attr('data-href'), null, null, inp.form, 'get')
            inp.previousSibling.value=this.attr('data-id');
            inp.nextElementSibling.hide();
        }
    }
    function autocomplete(event){
        var keyCode = event.keyCode ? event.keyCode : event.which;
        if(this.value.length>2 && keyCode!=13 && keyCode!=38 && keyCode!=40){
            var url = this.attr('data-href');
            var p = 'type='+this.attr('data-type')+'&value='+this.value;

            this.ajax(url, p, function (node, response){
                //випадаючий список
                response=JSON.parse(response);
                var l = response.data.html
                var list=node.nextElementSibling;
                if(l > ''){
                    list.innerHTML = l;
                    list.show();
                    list.$('tr', trSelect);
                } else {
                    list.hide();
                }

            });
        }
    }
    function handleKey(event, inp) {
        var keyCode = event.keyCode ? event.keyCode : event.which;
        if(keyCode==13 || keyCode==38 || keyCode==40){
            var active=inp.nextElementSibling.$('tr.active');
            if(active){
                switch(keyCode){
                    case 13:
                        active.click();
                    break;
                    case 40:
                        newActive=active.nextElementSibling;
                        if(newActive){
                            active.removeClass('active');
                            newActive.addClass('active');
                        }
                    break;
                    case 38:
                        var newActive=active.previousSibling;
                        if(newActive){
                            active.removeClass('active');
                            newActive.addClass('active');
                        }
                    break;
                }
            }
            event.preventDefault();
            event.stopPropagation();
            return false;
        } else return true;
    }
    inp.onkeydown=function(event){ return handleKey(event, this);}
    inp.onkeyup=autocomplete.debounce(600);
    inp.onblur=function(){setTimeout(function(){if(inp.nextElementSibling)inp.nextElementSibling.hide();}, 100);}
}

function openParent(el){
    //TODO: save opened lists in cookie and reopen when page is reloaded. store in redis too
    el.onclick=function(){
        if(this.parentNode.hasClass('_opened'))
            this.parentNode.removeClass('_opened');
        else
            this.parentNode.addClass('_opened');
        return false;
    }
}

function removeParent(el){
    el.onclick=function(){
        this.parentNode.remove()
    }
}

function tableSearchField(inp){
    function formSubmit(event){
        if(inp.value.length > 2 && inp.value != inp.attr('data-old-value')){
            inp.attr('data-old-value', inp.value)
            submit(this.form);
        }
    }
    inp.onchange = formSubmit.debounce(600);
    inp.onkeyup = formSubmit.debounce(600);
}

function initAjax(parent){
    parent.$('form', initSubmit);
    parent.$('a', initLink);
    parent.$('.active-table tr', initRow);
    parent.$('._dropdown', initDropDown, {parentOpenClass: 'open'});
    parent.$('._WYSIWYG', initEditor);
    parent.$('._ac', ac);
    parent.$('._lu', lookup);
    parent.$('._open_parent', openParent);
    parent.$('._remove_parent', removeParent);
    parent.$('._tabs>ul>li', initTab);
    parent.$('._tagfield', tagAC);
    parent.$('._table_search input', tableSearchField);
}

initAjax(document.body);