/*AJAX*/
function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}
/*SELECTOR*/
function $(selector, callback, params){
    if(this.document)objects=document.querySelectorAll(selector);
    else objects=this.querySelectorAll(selector);
    if(objects.length>0){
        if(callback)for(i=0; i<objects.length; i++){
            if(params)callback(objects.item(i), params);
            else callback(objects.item(i));
        }
        if(objects.length==1)return objects[0]; else return objects;
    } else return false;
}
/*ELEMENT  */
Element.prototype.$=$;
Element.prototype.removeClass=function(className){
    if(this.hasClass(className)) {
        var reg = new RegExp('(\\s|^)'+className+'(\\s|$)');
        this.className=this.className.replace(reg,' ');
    }
}
Element.prototype.hasClass=function(className){
    return this.className.match(new RegExp('(\\s|^)'+className+'(\\s|$)'));
}
Element.prototype.addClass=function(className){
    if(!this.hasClass(className)) this.className += " "+className;
}
Element.prototype.enable=function(){this.disabled=false;}
Element.prototype.disable=function(){this.disabled=true;}
Element.prototype.show=function(){this.style.display="block";}
Element.prototype.hide=function(){this.style.display="none";}
Element.prototype.after=function(html){this.innerHTML+=html;}
Element.prototype.remove=function(){this.parentNode.removeChild(this);}
Element.prototype.insertElementBefore=function(el){
    var p = this.parentNode;
    p.insertBefore(el, this)
}
Element.prototype.insertElementAfter=function(el){
    var p = this.parentNode;
    p.insertBefore(el, this.nextElementSibling)
}
Element.prototype.index=function(){
    var list = this.parentNode.children;
    for (var i = 0, len = list.length; i < len; i++) {
        if (this === list[i]) {
            return i;
        }
    }
}
Element.prototype.attr=function(attr, val){
    if(!val)return this.getAttribute(attr);
    else if(val=='')this.removeAttribute(attr);
    else this.setAttribute(attr, val);
}
/*NODELIST*/
NodeList.prototype.enable=function(){for(i=0; i<this.length; i++)this.item(i).enable();}
NodeList.prototype.disable=function(){for(i=0; i<this.length; i++)this.item(i).disable();}
NodeList.prototype.addClass=function(className){
    for(i=0; i<this.length; i++)this.item(i).addClass(className);
}
NodeList.prototype.removeClass=function(className){
    for(i=0; i<this.length; i++)this.item(i).removeClass(className);
}
NodeList.prototype.remove=function(){
    for(i=0; i<this.length; i++)this.item(i).remove();
}
NodeList.prototype.show=function(){for(i=0; i<this.length; i++)this.item(i).show();}
NodeList.prototype.hide=function(){for(i=0; i<this.length; i++)this.item(i).hide();}
/*FUNCTION*/
Function.prototype.defer = function(timeout, ctx, args) {
    var that = this;
    return setTimeout(function() {
        that.apply(ctx, args || []);
    }, timeout);
}
Function.prototype.debounce = function(delay, ctx, args) {
    var fn = this, timer;
    return function() {
        var args = arguments, that = this;
        clearTimeout(timer);
        timer = setTimeout(function() {
            fn.apply(ctx || that, args);
        }, delay);
    };
}
Element.prototype.animate=function(property, unit, value, time){
	if(!time)time=500;
	var time2=45;
	var stepCount=parseInt(time/time2);
	var el=this;
	if(!el.style[property])el.style[property]=0+unit;
	var startValue=parseInt(el.style[property]);
	if(value!=startValue){
		var propStep=parseInt((value-startValue)/stepCount);
		var tID=setInterval(function(){
			var current=parseInt(el.style[property]);
			if( (current+propStep>value && propStep>0) || (current+propStep<value && propStep<0) ){
				clearInterval(tID);
				el.style[property]=value+unit;
			} else el.style[property]=parseInt(el.style[property])+propStep+unit;
		}, time2);
	}
}
function strstr (haystack, needle, bool) {
    var pos = 0;
    haystack += '';
    pos = haystack.indexOf(needle);
    if (pos == -1) {
        return haystack;
    } else {
        if (bool) {
            return haystack.substr(0, pos);
        } else {
            return haystack.slice(pos);
        }
    }
}
function index(parent, selector, element){
    var list = parent.$(selector);
    for (var i = 0, len = list.length; i < len; i++) {
        if (element === list[i]) {
            return i;
        }
    }
}