/*AJAX*/
var SITE_NAME='m2.lviv.ua';
function ajax(url, params, callback, form){
    beforeLoad();
    if(callback)var node=this;
    if(form)var method=this.method; else var method='post'
    var xhr = getXmlHttp()
    url=url.replace(location.host, location.host+'/ajax');
    if(method=='get'){
        url=strstr(url, '?', true);
        xhr.open(method, url+'?'+params, true);
        xhr.send();    
    } else {
        xhr.open(method, url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
    }
    xhr.onload=function(){
        if(callback){
            callback(node, xhr.response);
        } else eval(xhr.response);
        afterLoad();
    }
}
function iajax(p, callback){
    var xhr = getXmlHttp()
    xhr.open('get', 'http://'+location.hostname+'/i.php/?'+p, true);
    xhr.send();
    xhr.onload=function(){
        callback(xhr.response);
    }
}
Element.prototype.ajax=ajax;

function sendForm(form, action, callback){
    beforeLoad();
    if(callback)var node=form;
    var formData = new FormData(form);
    var method=form.method;
    var xhr = new XMLHttpRequest();
    xhr.open(method, action, true);
    xhr.onload = function(e) {
        if(callback){
            callback(node, xhr.response);
        } else eval(xhr.response);
        afterLoad();
    }
    xhr.send(formData);
    return false;
}
function ac(inp){
    function acSelect(li){
        li.onclick=function(){
            inp.value=this.attr('data-name');
            $('#'+inp.name+'_id').value=this.attr('data-id');
            inp.nextSibling.hide();
            inp.set.value=this.attr('data-setname');
            inp.set_int.value=this.attr('data-setid');
            if(inp.child)inp.child.focus();
        }
    }
    function autocomplete(event){
        var keyCode = event.keyCode ? event.keyCode : event.which;
        if(this.value.length>2 && keyCode!=13 && keyCode!=38 && keyCode!=40){
            var p='action=ac&type='+this.attr('data-type')+'&value='+this.value;
            if(!this.relation)relation(this);
            if(this.parent)p+='&parent_value='+this.parent_int.value;
            inp.set.value='';
            inp.set_int.value=0;
            
            var url='http://'+location.hostname+'/i/';
            this.ajax(url, p, function (node, data){
                //випадаючий список
                var list=node.nextSibling;
                list.innerHTML=data;
                list.show();
                list.$('li', acSelect);
            });
        } else {
            $('#'+inp.name+'_id').value=0;
        }
    }
    function handleKey(event, inp) {
        var keyCode = event.keyCode ? event.keyCode : event.which;
        if(keyCode==13 || keyCode==38 || keyCode==40){
            var active=inp.nextSibling.$('li.active');
            if(active){
                switch(keyCode){
                    case 13:
                        active.click();
                    break;
                    case 40:
                        newActive=active.nextSibling;
                        if(newActive){
                            active.removeClass('active');
                            newActive.addClass('active');
                        }
                    break;
                    case 38:
                        var newActive=active.previousSibling;
                        if(newActive){
                            active.removeClass('active');
                            newActive.addClass('active');
                        }
                    break;
                }
            }
            event.preventDefault();
            event.stopPropagation();
            return false;
        } else return true;
    }
    function relation(inp){
        inp.relation=true;
        inp.parent=$('#'+inp.attr('data-parent')+'_ac');
        inp.parent_int=$('#'+inp.attr('data-parent')+'_id');
        inp.child=$('#'+inp.attr('data-child')+'_ac');
        inp.child_int=$('#'+inp.attr('data-child')+'_id');
        inp.set=$('#'+inp.attr('data-set'));
        inp.set_int=$('#'+inp.attr('data-set')+'_id');
    }
    inp.onkeydown=function(event){ return handleKey(event, this);}
    inp.onkeyup=autocomplete.debounce(600);
    inp.onblur=function(){setTimeout(function(){if(inp.nextSibling)inp.nextSibling.hide();}, 100);}
};
function range2(el) {
    var mininp = el.getElementsByClassName('_min')[0];
    var maxinp = el.getElementsByClassName('_max')[0];
    var minval = el.getElementsByClassName('_minval')[0];
    var maxval = el.getElementsByClassName('_maxval')[0];
    var box = el.getElementsByClassName('_box')[0];
    var d = parseInt((maxinp.max - maxinp.min) / 50);
    mininp.onchange = function() {
        if (parseInt(this.value) > parseInt(maxinp.value))
            this.value = parseInt(maxinp.value) - d;
        minval.value = this.value;
    }
    minval.onchange = function() {
        if (parseInt(this.value) > parseInt(maxinp.value))
            this.value = parseInt(maxinp.value) - d;
        mininp.value = this.value;
    }
    maxinp.onchange = function() {
        if (parseInt(this.value) < parseInt(mininp.value))
            this.value = parseInt(mininp.value) + d;
        maxval.value = this.value;
    }
    maxval.onchange = function() {
        if (parseInt(this.value) < parseInt(mininp.value))
            this.value = parseInt(mininp.value) + d;
        maxinp.value = this.value;
    }
    box.onmousemove = function(e) {
        var mn = parseInt(maxinp.min);
        var l = parseInt(maxinp.max - maxinp.min);
        var center = parseInt(maxinp.value) / 2 + parseInt(mininp.value) / 2 - mn;
        mouseX = e.pageX - this.offsetLeft-parseInt(this.offsetParent.offsetLeft);
        var mousepos = mouseX / this.offsetWidth;
        if (center / l < mousepos)
            maxinp.style.zIndex = 3;
        else
            maxinp.style.zIndex = 1;
    }
}
function submit(form, callback){
    if(!callback)callback=null;
    
    if(form.attr('action')>'')var action=form.attr('action'); else {
        var action=location.href;
    }
    if(form.enctype=='multipart/form-data'){
        action=action.replace(SITE_NAME, SITE_NAME+'/ajax');
        sendForm(form, action, callback);
    } else {
        var params=serialize(form);
        form.ajax(action, params, callback, true);
    }
}
function initSubmit(form){
    form.onsubmit=function(){
        if(!this.hasClass('_activeForm'))submit(this);
        return false;
    }
}

function tab(tab){
    tab.onclick=function(){
        var p=this.parentNode.parentNode.parentNode;
        var id=this.attr('data-box');
        var className='.'+this.attr('data-class');
        p.$(className+' input, '+className+' select').disable();
        p.$(className).hide();
        $('#'+id).show();
        $('#'+id+' input, #'+id+' select').enable();
    }
}

function filter(form){
    function loadItems(){
        submit(form, function(){
            window.scrollTo(0, $('#menuBox').offsetHeight);
        });
    }
    form.onchange=loadItems.debounce(1000);
}

function initAjax(parent){
    //resize();
    var links = $('a');
    for (var i = 0; i < links.length; i++){
        if(links[i].hasClass('_show')){
            links[i].onclick=function(){
                var box=$(this.attr('href'));
                if(box.style.display!='block'){
                    if($('._popup'))$('._popup').hide();
                    box.show();
                    var captcha=box.$('.captcha');
                    if(captcha)captcha.src+=1;
                } else box.hide();
                return false;
            }
        } else if(links[i].hasClass('_hide')){
            links[i].onclick=function(){
                var box=$(this.attr('href'));
                box.hide();
                return false;
            }
        } else if(links[i].hasClass('_oauth')){
            links[i].onclick=function(){
                window.open(this.href, 'oauth', "height=430,width=600,menubar=no,location=no,resizable=no,scrollbars=no,status=no");
                return false;
            }
        } else if(links[i].hasClass('_content1')){
            links[i].onclick=function(){
                $('#content').className='content1';
                return false;
            }
        } else if(links[i].hasClass('_reload')){

        } else {
            links[i].onclick=function(){
                ajax(this.href, true);
                return false;
            }
        }
    }
    $('.close', function(box){box.onclick=function(){if(this.parentNode.id=='popup')this.parentNode.innerHTML=''; else this.parentNode.hide();}});
    $('form', initSubmit);
    $('#filterForm', filter);
    $('._ac', ac);
    $('._tab', tab);
    if($('#niceEditor'))new nicEditor({fullPanel : true, maxHeight : 130}).panelInstance('niceEditor');
    if($('.images'))imgView();
    //new DragObject($('#rect2'), false, function(){$('.nohover').show();}, function(){$('.nohover').hide();}, true);
    //слайдер; вихід за межі екрану
    $('#rect2 .green', showAndCalcPlace);
    $('#rect2 .white').onclick=hideRect2;
    $('._activeField', ajaxInput);
    $('._cropImage', function(inp){inp.onchange=handleFileSelect; return false;});
    $('._range2', range2);
}
function handleFileSelect() {
    cropper.start(document.getElementById("cropCanvas"), 1);
    // this function will be called when the file input below is changed
    var file = this.files[0];  // get a reference to the selected file

    var reader = new FileReader(); // create a file reader
    // set an onload function to show the image in cropper once it has been loaded
    reader.onload = function(event) {
        $('#cropBox').show();
        var data = event.target.result; // the "data url" of the image
        cropper.showImage(data); // hand this to cropper, it will be displayed
        cropper.startCropping();
    };

    reader.readAsDataURL(file); // this loads the file as a data url calling the function above once done
}
function uploadCanvasImage(url, name){
    var file=cropper.getCroppedImageBlob();
    if (file) {
        ajaxFileUpload(url, name, file);
    }
}
function ajaxFileUpload(url, name, file){
    var formData = new FormData();
    formData.append(name, file);
    var xhr = new XMLHttpRequest();
    xhr.open('post', url.replace(SITE_NAME, SITE_NAME + '/ajax'), true);
    xhr.onload = function(e) {
        eval(xhr.response);
    }
    xhr.send(formData);
}
function ajaxInput(inp){
    inp.addClass('disabled');
    if(inp.type=='file'){
        inp.onchange=function(){
            ajaxFileUpload(this.form.action, this.name, this.files[0]);
        }
    } else {
        inp.onmouseover=function(){
            this.removeClass('disabled');
        }
        inp.onmouseout=function(){
            this.addClass('disabled');
        }
        inp.onkeyup=function(event){
            var keyCode = event.keyCode ? event.keyCode : event.which;
            if(keyCode==13)this.blur();
        }
        inp.onblur=function(){
            ajax(this.form.action, this.name+'='+this.value);
            this.addClass('disabled');
            return false;
        }
    }
}
function hideRect2(){
    $('#rect2 .rect2Box').hide();
    $('#rect2').animate('marginTop', 'px', 0, 300);
    $('#rect2').animate('marginRight', 'px', 0, 300);
}
function showAndCalcPlace(button) {
    button.onclick = function() {
        if(document.body.offsetWidth<800)return;
        var box = button.$('.rect2Box');

        if (box && box.style.display != 'block' && !button.hasClass('empty')) {
            button.parentNode.$('.rect2Box').hide();

            button.parentNode.animate('marginTop', 'px', 0, 300);
            button.parentNode.animate('marginRight', 'px', 0, 300);

            setTimeout(function() {

                var w = 480, h = 480, dr = 47, dt = 50;
                var r=document.body.offsetWidth-button.parentNode.offsetLeft-160;
                var t = button.parentNode.offsetTop;

                if (box.hasClass('note')) {
                    var mt = 275;
                    var mr = 0;
                } else if (box.hasClass('contact')) {
                    var mt = 0;
                    var mr = 0;
                } else if (box.hasClass('login') || box.hasClass('profile')) {
                    var mt = 275;
                    var mr = 275;
                } else if (box.hasClass('register')) {
                    var mt = 0;
                    var mr = 275;
                }

                box.style.width = '0px';
                box.style.height = '0px';
                box.animate('width', 'px', 400);
                box.animate('height', 'px', 400);
                
                button.parentNode.animate('marginTop', 'px', mt - t + dt);
                button.parentNode.animate('marginRight', 'px', mr - r + dr);

                box.show();

            }, 350);
        }
    }
}
/*UI*/
function notice(text, type){
    $('#notice').innerHTML='<span class="'+type+'">'+text+'</span>';
    setTimeout(function(){$('#notice').innerHTML='';}, 5000);
}
function popup(html, title, link){
    $('#content').className='content2';
    $('#content2').innerHTML=html;
    initAjax($('#after'));
    history.pushState({}, title, link);
}
function content(html, title, link){
    $('#content').className='content1';
    $('#content1').innerHTML=html;
    initAjax($('#content1'));
    history.pushState({}, title, link);
}
function items(html, title, link){
    if($('#popup').length>0)$('#popup').remove();
    $('#objects').innerHTML=html;
    initAjax($('#objects'));
    history.pushState({}, title, link);
}
/*INIT*/
onload=initAjax(document.body);