function get_avatar_from_service(service, userid, size) {
    // this return the url that redirects to the according user image/avatar/profile picture
    // implemented services: google profiles, facebook, gravatar, twitter, tumblr, default fallback
    // for google   use get_avatar_from_service('google', profile-name or user-id , size-in-px )
    // for facebook use get_avatar_from_service('facebook', vanity url or user-id , size-in-px or size-as-word )
    // for gravatar use get_avatar_from_service('gravatar', md5 hash email@adress, size-in-px )
    // for twitter  use get_avatar_from_service('twitter', username, size-in-px or size-as-word )
    // for tumblr   use get_avatar_from_service('tumblr', blog-url, size-in-px )
    // everything else will go to the fallback
    // google and gravatar scale the avatar to any site, others will guided to the next best version
    var url = '';

    switch (service) {

    case "google":
        // see http://googlesystem.blogspot.com/2011/03/unedited-google-profile-pictures.html (couldn't find a better link)
        // available sizes: all, google rescales for you
        url = "http://profiles.google.com/s2/photos/profile/" + userid + "?sz=" + size;
        showAvatar(url);
        break;

    case "facebook":
        // see https://developers.facebook.com/docs/reference/api/
        // available sizes: square (50x50), small (50xH) , normal (100xH), large (200xH)
        var sizeparam = '';
        if (isNumber(size)) {
            if (size >= 200) {
                sizeparam = 'large'
            };
            if (size >= 100 && size < 200) {
                sizeparam = 'normal'
            };
            if (size >= 50 && size < 100) {
                sizeparam = 'small'
            };
            if (size < 50) {
                sizeparam = 'square'
            };
        } else {
            sizeparam = size;
        }
        url = "https://graph.facebook.com/" + userid + "/picture?type=" + sizeparam;
        showAvatar(url);
        break;

    case "twitter":
        // see https://dev.twitter.com/docs/api/1/get/users/profile_image/%3Ascreen_name
        // available sizes: bigger (73x73), normal (48x48), mini (24x24), no param will give you full size
        var sizeparam = '';
        if (isNumber(size)) {
            if (size >= 73) {
                sizeparam = 'bigger'
            };
            if (size >= 48 && size < 73) {
                sizeparam = 'normal'
            };
            if (size < 48) {
                sizeparam = 'mini'
            };
        } else {
            sizeparam = size;
        }

        url = "http://api.twitter.com/1/users/profile_image?screen_name=" + userid + "&size=" + sizeparam;
        showAvatar(url);
        break;

    case "vk":
        var sizeparam = '';
        if (size >= 201) {
            sizeparam = 400
        };
        if (size >= 101 && size < 201) {
            sizeparam = 100
        };
        if (size >= 51 && size < 101) {
            sizeparam = 100
        };
        if (size < 51) {
            sizeparam = 50
        };
        
        iajax('action=avatar&id='+userid+'&size='+sizeparam, function(url){showAvatar(url)});
    }


    
}
function showAvatar(url){
    alert(url);
}

// helper methods

function isNumber(n) {
    // see http://stackoverflow.com/questions/18082/validate-numbers-in-javascript-isnumeric
    return !isNaN(parseFloat(n)) && isFinite(n);
}

get_avatar_from_service('facebook', 160384530772693, 100);