import os.path
import re
import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import unicodedata
from tornado.options import define, options
import psycopg2
import momoko
import logging
import logging.handlers
# from gcm import GCM

define("port", default=8105, help="run on the given port", type=int)
define("redis_port", default=6379, help="redis on the given port", type=int)
define("postgresql_config", default="dbname=dcall user=m password=CrkflybqGfhjkmYfGjcnuht1992 host=localhost port=5432", help="database config")


class Application(tornado.web.Application):
    def __init__(self):
        lang_url = '/(ua|en|pl|ru)?'
        handlers = [
            # public
            ("%s/?", h_.HomeHandler),
            ("%s/login/?", h_.LoginHandler),
            ("ajax%s/login/?", h_.LoginHandler),
            ("%s/logout/?", h_.LogoutHandler),
            ("%s/register/?", h_.RegisterHandler),
            #  admin
            ("%s/admin/?", ha_.DashboardHandler),
            # system
            #(r'/(.*)', h_.ErrorHandler),
            ("/ajax%s/ac/?", hi_.AutoCompleteHandler),
            ("/ajax%s/lu/?", hi_.LookupHandler),
            ("/ajax%s/lu/([^/]+)/([^/]+)/?", hi_.LookupAutoFill),
        ]
        admin_obj_handlers = [
            ("%s/admin/contacts/?", ha_.ContactsHandler),
            ("%s/admin/contact/([^/]+)/?", ha_.ContactHandler),
            ("%s/admin/calls/?", ha_.CallsHandler),
            ("%s/admin/call/([^/]+)/?", ha_.CallHandler),
            ("%s/admin/messages/?", ha_.MessagesHandler),
            ("%s/admin/message/([^/]+)/?", ha_.MessageHandler)
        ]
        no_lang_handlers = [
            ("/fb_login/?", h_.FBLoginHandler),
            ("/vk_login/?", h_.VKLoginHandler),
            ("/li_login/?", h_.LILoginHandler),
            ("/register/?", ha_.AddContactHandler),
            ("/call/?", ha_.AddCallHandler),
            ("/sms/?", ha_.AddSMSHandler),
            ("/site_call/?", ha_.SiteCallHandler)
        ]
        ajax_admin_obj_handlers = [('/ajax'+k, h) for k, h in admin_obj_handlers]
        admin_obj_handlers += ajax_admin_obj_handlers
        handlers += admin_obj_handlers
        lang_handlers = [(k % (lang_url,), h) for k, h in handlers]
        all_handlers = lang_handlers + no_lang_handlers

        settings = dict(
            site_title="D-call",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            ui_modules={"Profile": m_.Profile, "Paginator": m_.Paginator, "Form": m_.Form, "LastPubTitles": m_.LastPubTitles, "SubMenu": m_.SubMenu},
            xsrf_cookies=False,
            cookie_secret="ghjcnbqgfhjkm",
            login_url="/ua/login",
            # gcm=GCM('AIzaSyAAl32yCktq8X_SlsJZhUjtU1gToZubJXA'),
            debug=True,
        )
        tornado.web.Application.__init__(self, all_handlers, **settings)

        self.db = momoko.Pool(dsn=options.postgresql_config, size=1)
        self.adminConfig = config.AdminConfig(self.db)
        self.user = user.User(self.db, None, self.adminConfig, salt=settings['cookie_secret'])


from core import user
from core import config
import handlers.base as h_
import handlers.admin as ha_
import handlers.public as hp_
import handlers.ajax as hi_
import modules.base as m_

if __name__ == "__main__":
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.locale.load_translations(os.path.join(os.path.dirname(__file__), "translations"))
    tornado.ioloop.IOLoop.instance().start()