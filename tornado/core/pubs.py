from core.object import Object


class Contact(Object):
    code = 'contact'
    code_plural = 'contacts'
    table = 'contacts'
    form_title = 'Edit contact'
    columns = {
        'id': {'type': 'Id'},
        'org': {'type': 'String'},
        'name': {'type': 'String'},
        'surname': {'type': 'String'},
        'phone_number': {'type': 'String'},
        'site': {'type': 'String'},
        'found_on': {'type': 'String'},
        'status': {'type': 'Int'},
        'comment': {'type': 'String'}
    }


class Call(Object):
    code = 'call'
    code_plural = 'calls'
    table = 'calls'
    columns = {
        'id': {'type': 'Id'},
        'name': {'type': 'String'},
        'time': {'type': 'Timestamp'},
        'duration': {'type': 'Int'},
        'is_coming': {'type': 'Bool'},
        'phone_number': {'type': 'String'},
        'site': {'type': 'String'},
        'comment': {'type': 'String'}
    }


class Message(Object):
    code = 'message'
    code_plural = 'messages'
    table = 'messages'
    columns = {
        'id': {'type': 'Id'},
        'phone2': {'type': 'String'},
        'time': {'type': 'Timestamp'},
        'message': {'type': 'String'},
        'comment': {'type': 'String'}
    }