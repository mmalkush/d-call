from core.object import Object
import tornado.web
import momoko
import psycopg2


class User(Object):
    queries = {
        'getByEmail': "SELECT id, email, password, u_type, name FROM api_users WHERE email = %s",
        'getBySocId': "SELECT user_id FROM api_oauth WHERE soc_id = %s AND social_network = %s",
        'getById': "SELECT id, email, password, u_type, name FROM api_users WHERE id = %s",
        'insertUser': "INSERT INTO api_users (name, surname, u_type) VALUES (%s, %s, %s) RETURNING id",
        'insertOauth': "INSERT INTO api_oauth (user_id, social_network, soc_id, soc_key) VALUES (%s, %s, %s, %s)",
        'updateOauth': "UPDATE api_oauth SET soc_key = %s WHERE user_id = %s AND social_network = %s AND soc_id = %s",
        'adminCount': "SELECT COUNT(*) FROM api_users WHERE u_type = 'admin'"
    }
    table = 'api_users'
    code = 'user'
    code_plural = 'users'
    id_key = 'id'
    _fullname = ''
    columns = {
        'id': {'type': 'Id'},
        'name': {'type': 'String', 'max_len': 50, 'min_len': 3},
        'surname': {'type': 'String', 'max_len': 50, 'min_len': 3},
        'email': {'type': 'Email'},
        'reg_id': {'type': 'String'},
        'auth_failed': {'type': 'Int'},
        'has_image': {'type': 'Image', 'user_type': 'site', 'image_name': 'id', 'ext': 'JPEG', 'w': 600, 'h': 480, 'sizes': {'m': (200, 200), 's': (100, 100)}}
        #'checked': {'type': 'Bool', 'user_type': 'moderator'},
    }

    def __init__(self, db, user, config_data, search_params=None, form_params=None, user_class=None, files=None, lang='ua', salt='', locales=None):
        Object.__init__(self, db, user, config_data, search_params, form_params, user_class, files, lang)
        self.salt = salt
        self._fullname = "%s.name||' '||%s.surname" % (self.table, self.table)

    def fullname(self):
        return "%s.name||' '||%s.surname as user_name" % (self.table, self.table)

    @tornado.gen.coroutine
    def oauth_login(self, handler, soc_network, soc_id, token, name, surname, picture='', email=''):
        cursor = yield momoko.Op(self.db.execute, self.queries['getBySocId'], (str(soc_id), soc_network), cursor_factory=psycopg2.extras.RealDictCursor)
        user_oauth = cursor.fetchone()
        if user_oauth:
            user_id = user_oauth['user_id']
            yield self.update_oauth(user_id, token, str(soc_id), soc_network)
        else:
            # register
            user_id = yield self.insert_user(name, surname, picture)
            yield self.insert_oauth(user_id, token, str(soc_id), soc_network)

        cursor = yield momoko.Op(self.db.execute, self.queries['getById'], (user_id,), cursor_factory=psycopg2.extras.RealDictCursor)
        user = cursor.fetchone()
        handler.set_current_user(user)

    @tornado.gen.coroutine
    def insert_user(self, name, surname, picture=None):
        cursor = yield momoko.Op(self.db.execute, self.queries['adminCount'])
        admin_number = cursor.fetchone()
        u_type = 'guest'
        if admin_number[0] < 2:
            u_type = 'admin'
        cursor = yield momoko.Op(self.db.execute, self.queries['insertUser'], (name, surname, u_type))
        return cursor.fetchone()[0]

    @tornado.gen.coroutine
    def insert_oauth(self, user_id, token, soc_id, social_network):
        yield momoko.Op(self.db.execute, self.queries['insertOauth'], (user_id, social_network, soc_id, token))

    @tornado.gen.coroutine
    def update_oauth(self, user_id, token, soc_id, social_network):
        yield momoko.Op(self.db.execute, self.queries['updateOauth'], (token, user_id, social_network, soc_id))