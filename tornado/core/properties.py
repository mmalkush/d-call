import core.html as html
import re
import time
import PIL.Image
from io import BytesIO
from transliterate import translit
from slugify import slugify
import os.path
import tornado.web
import momoko
import psycopg2
from tornado.httpclient import AsyncHTTPClient
import time
import datetime

TAG_RE = re.compile(r'<[^>]+>')


class Property():
    name = ''
    parent = None
    data = {}
    input_type = 'text'
    show_label = True
    label = ''
    data_preload = False
    load_objects = False
    _after_save = False
    sortable = False

    def __init__(self, name, parent, data={}):
        self.name = name
        self.parent = parent
        self.data = data
        if self.show_label:
            self.label = '{{ _("%s") }}' % self.name

    def html(self):
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            val = self.parent.row_data[self.name]
        else:
            val = ''

        field = html.field(self.name, self.input_type, val, self.label, self.name, 'div')
        return field

    def validate_search(self):
        if self.name in self.parent.search_params:
            self.parent.search_params[self.name] = TAG_RE.sub('', self.parent.search_params[self.name])
        else:
            self.parent.search_params[self.name] = ''

    def validate_form(self):
        # check rights
        if self.name in self.parent.form_params:
            self.parent.form_params[self.name] = TAG_RE.sub('', self.parent.form_params[self.name])

    def th_search(self):
        field = html.field(self.name, self.input_type, self.parent.search_params[self.name], "", self.name)
        return '<th>%s</th>' % (field,)

    def th_title(self):
        order_type = 'DESC'
        if 'order_by' in self.parent.search_params and self.parent.search_params['order_by'] == self.name:
            if 'order_type' in self.parent.search_params:
                if self.parent.search_params['order_type'] == 'DESC':
                    order_type = 'ASC'
        search_params = ''
        if self.parent.search_options:
            search_params = '&' + self.parent.search_options
        return '<th><a href="/%s%s&order_by=%s&order_type=%s%s">{{ _("%s") }}</a></th>' % (self.parent.admin_table_uri, '', self.name, order_type, search_params, self.name)

    def td(self):
        return '<td>%s</td>' % (self.parent.row_data[self.name],)

    def where_sql(self):
        if self.name in self.parent.search_params and self.parent.search_params[self.name]:
            return "lower(" + self.name + ") LIKE '%%" + self.parent.search_params[self.name].lower() + "%%'"
        else:
            return ''

    def join_table(self):
        return ''

    def select_sql(self):
        return self.parent.table + '.' + self.name

    def insert_key(self):
        if self.name in self.parent.form_params:
            return self.name
        else:
            return None

    def insert_value(self):
        if self.name in self.parent.form_params and self.parent.form_params[self.name]:
            return int(self.parent.form_params[self.name])
        else:
            return None

    def update_key(self):
        if self.name in self.parent.form_params:
            return self.name
        else:
            return None

    def update_value(self):
        if self.name in self.parent.form_params and self.parent.form_params[self.name]:
            return int(self.parent.form_params[self.name])
        else:
            return None

    def get_sort(self):
        if 'order_by' in self.parent.search_params and self.name == self.parent.search_params['order_by']:
            order_type = 'DESC'
            if 'order_type' in self.parent.search_params and self.parent.search_params['order_type'] != order_type:
                order_type = 'ASC'
            return self.parent.table + '.' + self.name + ' ' + order_type
        else:
            return ''

    @tornado.gen.coroutine
    def after_save(self):
        return
        yield


class String(Property):
    def insert_value(self):
        if self.name in self.parent.form_params:
            return self.parent.form_params[self.name]
        else:
            return None

    def update_value(self):
        if self.name in self.parent.form_params:
            return self.parent.form_params[self.name]
        else:
            return None


class Number(Property):
    input_type = 'number'


class Code(String):
    pass


class StringOriginal(String):
    show_label = False

    def html(self):
        html_str = '<div class="tc-tabs _tabs"><ul class="nav-tabs tab-color-dark background-dark">%s</ul><div class="tab-content">%s</div>'
        li_str = ''
        panel_str = ''
        locales = list(self.parent.locales)
        locales.sort(reverse=True)
        for code in locales:
            f_name = self.name[:-2] + code
            # title
            if self.name == f_name:
                cls = 'active'
            else:
                cls = ''
            label = '{{ _("%s") }}' % f_name
            li_str += '<li class="%s">%s</li>' % (cls, label)
            # body
            if f_name in self.parent.row_data and self.parent.row_data[f_name]:
                val = self.parent.row_data[f_name]
            else:
                val = ''
            field = self.tab_content(f_name, val, cls, code)
            panel_str += '<div class="tab-pane %s">%s</div>' % (cls, field)

        html_str %= (li_str, panel_str)

        return html_str

    def tab_content(self, name, val, cls=None, lang=None):
        return html.field(name, self.input_type, val)


class StringTranslate(String):
    def html(self):
        return ''


class Email(String):
    input_type = 'email'


class Password(Property):
    input_type = 'password'


class Text(String):
    classname = ""

    def html(self):
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            val = self.parent.row_data[self.name]
        else:
            val = ''
        field = html.textarea(self.name, val, self.label, self.name, 'div', self.classname)
        return field

    def validate_form(self):
        pass


class TextOriginal(StringOriginal):
    def tab_content(self, name, val, cls, lang=None):
        return html.textarea(name, val, classname=self.classname)

    def validate_form(self):
        pass


class TextTranslate(StringTranslate):
    def validate_form(self):
        pass


class HtmlText(Text):
    classname = "_WYSIWYG"
    pass


class HtmlOriginal(StringOriginal):
    classname = "_WYSIWYG"

    def validate_form(self):
        pass

    def tab_content(self, name, val, cls, lang=None):
        return html.textarea(name, val, classname=self.classname)


class HtmlTranslate(StringTranslate):
    classname = "_WYSIWYG"

    def validate_form(self):
        pass


class Hash(Property):
    pass


class RandomInt(Property):
    pass


class Id(Property):
    input_type = "hidden"

    def html(self):
        if self.name in self.parent.row_data:
            val = self.parent.row_data[self.name]
        else:
            val = ''
        field = html.field(self.name, self.input_type, val, "", self.name, 'div')
        return field

    def validate_search(self):
        if self.name not in self.parent.search_params:
            self.parent.search_params[self.name] = ''

    def validate_form(self):
        # check rights
        if self.name in self.parent.form_params and self.parent.form_params[self.name]:
            self.parent.form_params[self.name] = int(self.parent.form_params[self.name])

    def th_search(self):
        return '<th class="col-small center"></th>'

    def th_title(self):
        return '<th class="col-small center">{{ _("%s") }}</th>' % (self.name,)

    def td(self):
        html_str = html.checkbox(self.name + '[]', self.parent.row_data[self.name], False)
        return '<td class="col-small center">%s</td>' % (html_str,)

    def where_sql(self):
        if self.name in self.parent.search_params and self.parent.search_params[self.name]:
            return self.name + " = %s" % self.parent.search_params[self.name]
        else:
            return None

    def insert_key(self):
        return None

    def insert_value(self):
        return None

    def update_key(self):
        return None

    def update_value(self):
        return None


class Created(Property):
    admin_format = '%Y-%m-%d %H:%M'  # 'time_ago'  # yesterday, 1 day ago, 15 days ago
    public_format = '%Y-%m-%d %H:%M'  # 'time_ago'  # yesterday, 1 day ago, 15 days ago
    hint_format = '%Y-%m-%d %H:%M'
    pass_format = '%Y-%m-%d %H:%M'
    db_format = 'YYYY-MM-DD HH24:MI'
    min_date = time.strptime('2014-01-01 00:00', '%Y-%m-%d %H:%M')
    max_date = time.time()

    def html(self):
        if self.name in self.parent.row_data:
            val = self.parent.row_data[self.name]
        else:
            val = ''
        field = html.field(self.name, "text", val, self.label, self.name, 'div')
        return field

    def validate_search(self):
        start_date = None
        end_date = None
        if self.name + '_start' in self.parent.search_params:
            start_date = time.strptime(self.parent.search_params[self.name + '_start'], '%Y-%m-%d %H:%M')
            if start_date > self.max_date or start_date < self.min_date:
                start_date = None
        if self.name + '_end' in self.parent.search_params:
            end_date = time.strptime(self.parent.search_params[self.name + '_end'], '%Y-%m-%d %H:%M')
            if end_date > self.max_date or end_date < self.min_date:
                end_date = None
        if start_date and end_date and start_date > end_date:
            d = start_date
            start_date = end_date
            end_date = d
        if start_date:
            self.parent.search_params[self.name + '_start'] = start_date.strftime(self.pass_format)
        else:
            self.parent.search_params[self.name + '_start'] = ''
        if end_date:
            self.parent.search_params[self.name + '_end'] = end_date.strftime(self.pass_format)
        else:
            self.parent.search_params[self.name + '_end'] = ''

    def validate_form(self):
        # check rights
        if self.name in self.parent.form_params:
            del self.parent.form_params[self.name]

    def th_search(self):
        field1 = html.field(self.name, "text", self.parent.search_params[self.name + '_start'], "", self.name + '_start')
        field2 = html.field(self.name, "text", self.parent.search_params[self.name + '_end'], "", self.name + '_end')
        return '<th>%s - %s</th>' % (field1, field2)

    def where_sql(self):
        start_sql = None
        end_sql = None
        if self.parent.search_params[self.name + '_start']:
            start_sql = self.name + " > %s" % self.parent.search_params[self.name + '_start']
        if self.parent.search_params[self.name + '_end']:
            end_sql = self.name + " < %s" % self.parent.search_params[self.name + '_end']
        if start_sql and end_sql:
            return start_sql + ' AND ' + end_sql
        else:
            if start_sql:
                return start_sql
            if end_sql:
                return end_sql
        return ''

    def select_sql(self):
        return self.parent.table + '.' + self.name

    def insert_key(self):
        return None

    def insert_value(self):
        return None

    def update_key(self):
        return None

    def update_value(self):
        return None


class Edited(Created):
    def update_key(self):
        return None  # self.name

    def update_value(self):
        return None  # b'CURRENT_TIMESTAMP'


class UserId(Property):
    user_name = 'user_name'

    def html(self):
        if self.name in self.parent.row_data:
            val = self.parent.row_data[self.user_name]
        else:
            val = ''
        field = html.field(self.name, "text", val, self.label, self.name, 'div')
        return field

    def validate_form(self):
        pass

    def th_search(self):
        field = html.field(self.name, "text", self.parent.search_params[self.name], "", self.name)
        return '<th>%s</th>' % (field,)

    # def th_title(self):
    # return '<th class="sorting"><a href="?" >{{ _("%s") }}</a></th>' % (self.name,)

    def td(self):
        return '<td>%s</td>' % (self.parent.row_data[self.user_name],)

    def where_sql(self):
        q = ' ' + self.parent.table + '.' + self.name + '=' + self.parent.user_class.table + '.' + self.parent.user_class.id_key
        if self.name in self.parent.search_params and self.parent.search_params[self.name]:
            q += " AND lower(" + self.parent.user_class._fullname + ") LIKE '%%" + self.parent.search_params[self.name].lower() + "%%'"
        return q

    def join_table(self):
        return ' LEFT JOIN %s ON %s=%s.%s' % (self.parent.user_class.table, self.name, self.parent.user_class.table, self.parent.user_class.id_key)

    def select_sql(self):
        query = self.parent.table + '.' + self.name
        query += ', ' + self.parent.user_class.fullname() + ''
        return query

    def insert_key(self):
        return self.name

    def insert_value(self):
        return int(self.parent.user['id'])

    def update_key(self):
        return None

    def update_value(self):
        return None

    def get_sort(self):
        if 'order_by' in self.parent.search_params and self.name == self.parent.search_params['order_by']:
            order_type = 'DESC'
            if 'order_type' in self.parent.search_params and self.parent.search_params['order_type'] != order_type:
                order_type = 'ASC'
            # return self.parent.user_class.table + '.' + self.parent.user_class.name + ' ' + order_type
            return ' user_name ' + order_type
        else:
            return ''


class Bool(Property):
    def html(self):
        checked = False
        if self.name in self.parent.row_data:
            checked = int(self.parent.row_data[self.name]) == 1
        field = html.checkbox(self.name, checked)
        return field

    def validate_search(self):
        if self.name in self.parent.search_params:
            self.parent.search_params[self.name] = 1

    def validate_form(self):
        # check rights
        if self.name in self.parent.form_params:
            self.parent.form_params[self.name] = bool(self.parent.form_params[self.name])

    def th_search(self):
        checked = self.name in self.parent.search_params
        field = html.checkbox(self.name, checked, checked)
        return '<th>%s</th>' % (field,)

    def td(self):
        checked = int(self.parent.row_data[self.name]) == 1
        html_str = html.checkbox(self.name, checked)
        return '<td>%s</td>' % (html_str,)

    def where_sql(self):
        if self.name in self.parent.search_params:
            return self.name + " = 1 "
        else:
            return ''

    def insert_key(self):
        if self.name in self.parent.form_params:
            return self.name
        else:
            return False

    def insert_value(self):
        if self.name in self.parent.form_params:
            return bool(self.parent.form_params[self.name])
        else:
            return False

    def update_key(self):
        if self.name in self.parent.form_params:
            return self.name
        else:
            return False

    def update_value(self):
        if self.name in self.parent.form_params:
            return bool(self.parent.form_params[self.name])
        else:
            return False


class Select(String):
    values = {}

    def get_values(self):
        # if 'min' in self.data and 'max' in self.data:
        # self.values = enumerate(range(self.data['min'], self.data['min']))
        if 'values' in self.data:
            self.values = self.data['values']

    def html(self):
        self.get_values()
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            val = self.parent.row_data[self.name]
        else:
            val = ''

        if 'required' in self.data:
            required = True
        else:
            required = None

        field = html.select(self.name, self.values, val, self.label, 'div', required)
        return field


class SelectEnum(Select):
    pass


class SelectColumn(Select):
    query = "SELECT %s as id, %s as label FROM %s LIMIT 100"
    data_preload = True

    @tornado.gen.coroutine
    def load_data(self):
        label_column = self.data['label']
        if 'translate_postfix' in self.data:
            label_column += self.parent.lang.code
        query = self.query % (self.data['id'], label_column, self.data['table'])
        cursor = yield momoko.Op(self.parent.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
        self.parent.properties_data[self.name] = cursor.fetchall()

    def get_values(self):
        self.values = self.parent.properties_data[self.name]


class Price(Property):
    def where_sql(self):
        min = self.name + '_min'
        if min in self.parent.search_params:
            return self.name + " > %s" % "'" + self.parent.search_params[min] + "'"
        else:
            return ''


class PriceM(Property):
    pass


class Int(Property):
    pass


class Autocomplete(Property):
    href = '/ac/'

    def html(self):
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            val_key = self.parent.row_data[self.name]
            val_label = self.parent.row_data[self.name + '_label']
        else:
            val_key = ''
            val_label = ''

        required = None
        if 'required' in self.data:
            required = True

        href = '/' + self.parent.lang.code + '/ac/'

        field = html.ac(self.data['ac_type'], self.name, href, val_key, val_label, '', '', self.name, required)
        return field

    def td(self):
        return '<td>%s</td>' % (self.parent.row_data[self.name + '_label'],)

    def join_table(self):
        ac_data = self.parent.autocomplete_array[self.data['ac_type']]
        return ' LEFT JOIN %s ON %s=%s.%s' % (ac_data['table'], self.name, ac_data['table'], ac_data['key'])

    def select_sql(self):
        ac_data = self.parent.autocomplete_array[self.data['ac_type']]
        query = self.parent.table + '.' + self.name
        columns = []
        for column in ac_data['label_columns']:
            columns.append(ac_data['table'] + '.' + column)
        label = " || ' ' || ".join(columns)
        query += ', ' + label + ' as ' + self.name + '_label'
        return query


class ObjectId(Autocomplete):
    def html(self):
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            val_label = self.parent.row_data[self.name + '_label']
        else:
            val_label = ''

        field = html.field(self.name, 'text', val_label, disabled=True)
        return field


class Rate(Property):
    pass


class Slug(Property):
    def insert_key(self):
        if self.data['source'] in self.parent.form_params:
            return self.name
        else:
            return None

    def update_key(self):
        if self.data['source'] in self.parent.form_params:
            return self.name
        else:
            return None

    def insert_value(self):
        if self.data['source'] in self.parent.form_params:
            return slugify(translit(self.parent.form_params[self.data['source']], 'uk'))
        else:
            return None

    def update_value(self):
        if self.data['source'] in self.parent.form_params:
            return slugify(translit(self.parent.form_params[self.data['source']], 'uk'))
        else:
            return None


class Clicks(Property):
    pass


class Views(Property):
    pass


class Image(Property):  # has image - boolean
    # self.data.image_name in [None (save original name), id (size + id_hash), slug]
    # additional sizes in self.data.sizes
    #  size = w x h
    input_type = 'file'
    _after_save = True

    def html(self):
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            file_name = '/' + self.parent.img_dir + 's/' + self.get_file_name()
        else:
            file_name = None

        field = html.image(self.name, file_name, self.label, self.name, 'div')
        return field

    def validate_search(self):
        pass

    def validate_form(self):
        #  validation
        pass

    def get_file_name(self, ext='jpg'):
        if self.data['image_name'] == 'id':
            return str(self.parent.item_id) + '.' + ext

    @tornado.gen.coroutine
    def after_save(self):
        if self.name in self.parent.files:
            file_body = self.parent.files[self.name][0]['body']
            img = PIL.Image.open(BytesIO(file_body))
            file_name = self.get_file_name()
            img.save(self.parent.img_dir + 'source/' + file_name, "JPEG")
            size = self.data['w'], self.data['h']
            img.thumbnail(size)
            img.save(self.parent.img_dir + file_name, "JPEG")

            if 'sizes' in self.data:
                for key in self.data['sizes']:
                    img.thumbnail(self.data['sizes'][key])
                    img.save(self.parent.img_dir + key + '/' + file_name, "JPEG")
        return
        yield

    def th_search(self):
        return '<th></th>'

    def th_title(self):
        return '<th class="sorting">{{ _("%s") }}</th>' % (self.name,)

    def td(self):
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            file_name = '/' + self.parent.img_dir + 's/' + self.get_file_name()
            return '<td class="image"><img src="%s" /></td>' % (file_name,)
        else:
            return '<td></td>'

    def where_sql(self):
        if self.name in self.parent.search_params and self.parent.search_params[self.name]:
            return self.name + " LIKE '%%%s%%'" % self.parent.search_params[self.name]
        else:
            return ''

    def insert_key(self):
        if self.name in self.parent.files:
            return self.name
        else:
            return None

    def insert_value(self):
        if self.name in self.parent.files:
            return True
        else:
            return None

    def update_key(self):
        if self.name in self.parent.files:
            return self.name
        else:
            return None

    def update_value(self):
        if self.name in self.parent.files:
            return True
        else:
            return None


class RequiredImage(Image):
    def insert_key(self):
        return None

    def insert_value(self):
        return None

    def update_key(self):
        return None

    def update_value(self):
        return None

    def select_sql(self):
        return ''

    def html(self):
        file_name = self.parent.img_dir + 's/' + self.get_file_name()
        if os.path.isfile(file_name):
            file_name = '/' + file_name
        else:
            file_name = None

        field = html.image(self.name, file_name, self.label, self.name, 'div')
        return field

    def td(self):
        file_name = self.parent.img_dir + 's/' + self.get_file_name()
        if os.path.isfile(file_name):
            return '<td><img src="/%s" /></td>' % (file_name,)
        else:
            return '<td></td>'


class Autoposting(Bool):
    _after_save = True

    @tornado.gen.coroutine
    def after_save(self):
        pass
        # http_client = AsyncHTTPClient()
        # response = yield http_client.fetch("http://example.com")

    def update_key(self):
        return False

    def update_value(self):
        return False

    def insert_key(self):
        return False

    def insert_value(self):
        return False


class Rooms(Property):
    pass


class RoomPhotos(Property):
    pass


class LookupOriginal(StringOriginal):
    href = '/lu/'

    def tab_content(self, name, val, cls=None, lang=None):
        lu_type = self.data['lu_type'].replace('_ua_', '_' + lang + '_')
        if cls == 'active':
            return html.field(name, self.input_type, val)
        else:
            return html.lookup(lu_type, name, val, '/' + self.parent.lang.code + self.href)


class NObjects(Property):
    def html(self):
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            values = self.parent.row_data[self.name]
        else:
            values = []
        values = set(values)
        all_values = self.get_values()
        field = self.get_field()
        return html.tags(field, self.name, values, all_values, lang=self.parent.lang.code, label=self.label)

    def get_field(self):
        return html.field(self.name, classname='_tagfield')

    def validate_search(self):
        if self.name not in self.parent.search_params:
            self.parent.search_params[self.name] = ''

    def validate_form(self):
        name = self.name + '[]'
        if name in self.parent.form_params:
            self.parent.form_params[name] = list(map(int, self.parent.form_params[name]))

    def th_search(self):
        field = html.field(self.name, self.input_type, self.parent.search_params[self.name], "", self.name)
        return '<th>%s</th>' % (field,)

    def th_title(self):
        return '<th>{{ _("%s") }}</th>' % (self.name,)

    def td(self):
        return '<td>%s</td>' % (self.parent.row_data[self.name],)

    def where_sql(self):
        # if self.name in self.parent.search_params and self.parent.search_params[self.name]:
        # return self.name + " LIKE '%%%s%%'" % self.parent.search_params[self.name]
        # else:
        return ''

    def insert_key(self):
        name = self.name + '[]'
        if name in self.parent.form_params:
            return self.name
        else:
            return None

    def insert_value(self):
        name = self.name + '[]'
        if name in self.parent.form_params:
            return self.parent.form_params[name]
        else:
            return None

    def update_key(self):
        if self.name in self.parent.form_params:
            return self.name
        else:
            return None

    def update_value(self):
        name = self.name + '[]'
        if self.name in self.parent.form_params:
            return self.parent.form_params[name]
        else:
            return None


class NObjectsSelect(NObjects):
    query = "SELECT %s as id, %s as label FROM %s LIMIT 100"
    data_preload = True
    values = {}
    show_values = {}

    @tornado.gen.coroutine
    def load_data(self):
        label_column = self.data['label']
        if 'translate_postfix' in self.data:
            label_column += self.parent.lang.code
        query = self.query % (self.data['id'], label_column, self.data['table'])
        cursor = yield momoko.Op(self.parent.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
        self.parent.properties_data[self.name] = cursor.fetchall()

    def get_values(self):
        self.values = self.parent.properties_data[self.name]

    def get_field(self):
        self.get_values()
        if self.name in self.parent.row_data and self.parent.row_data[self.name]:
            val = self.parent.row_data[self.name]
        else:
            val = []

        if self.values:
            for v in self.values:
                if v['id'] not in val:
                    self.show_values[v['id']] = v['label']

        field = html.select(self.name, self.show_values)
        return field


class NObjectsAutocomplete(NObjects):
    load_objects = True
    query = "SELECT %s as id, %s as label FROM %s WHERE %s IN (%s)"

    @tornado.gen.coroutine
    def load_objects(self):
        if self.name in self.parent.row_data and len(self.parent.row_data[self.name]) > 0:
            ids = self.parent.row_data[self.name]
            ac_data = self.parent.autocomplete_array[self.data['ac_type']]
            label_column = " || ' ' || ".join(ac_data['label_columns'])
            if 'translate_postfix' in ac_data and label_column in list(self.parent.locales):
                label_column += ', ' + self.parent.lang.code
            # query = self.query.replace('{id}', ac_data['key']).replace('{label}', label_column).replace('{table}', ac_data['table'])list(map(int, self.parent.form_params[name]))
            query = self.query % (ac_data['key'], label_column, ac_data['table'], ac_data['key'], ', '.join(list(map(str, ids))))
            cursor = yield momoko.Op(self.parent.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
            self.parent.linked_objects_data[self.name] = cursor.fetchall()

    def get_field(self):
        href = '/' + self.parent.lang.code + '/ac/'
        return html.ac('tags', self.name, href, classname='_tagfield')

    def get_values(self):
        if self.name in self.parent.linked_objects_data:
            return self.parent.linked_objects_data[self.name]
        else:
            return []


class Date(String):
    input_type = 'date'

    def th_search(self):
        # TODO: date_from and date_to fields
        field = html.field(self.name, self.input_type, self.parent.search_params[self.name], "", self.name)
        return '<th>%s</th>' % (field,)


class GoogleAddress(Autocomplete):
    pass


class Connected(Property):
    load_objects = True
    query = "select %s from %s C, %s T where %s"

    @tornado.gen.coroutine
    def load_objects(self):
        select_sql = 'T.' + ', T.'.join(self.data['data'])
        conditions = []
        rules = self.data['connector']['rules']
        for rule in rules:
            if rule[0:1] == ':':
                rule_val = self.parent.row_data[rule[1:]]
            else:
                rule_val = rules[rule]
            rule_sql = 'C.%s = %s' % (rule, rule_val)
            conditions.append(rule_sql)
        join_sql = 'C.%s = T.%s' % (self.data['connector']['key'], self.data['key'])
        conditions.append(join_sql)
        where_sql = ' AND '.join(conditions)
        query = self.query % (select_sql, self.data['connector']['table'], self.data['table'], where_sql)
        cursor = yield momoko.Op(self.parent.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
        self.parent.linked_objects_data[self.name] = cursor.fetchall()

    def update_key(self):
        return False

    def update_value(self):
        return False

    def insert_key(self):
        return False

    def insert_value(self):
        return False


class Timestamp(Int):
    def insert_key(self):
        if self.name in self.parent.form_params:
            return self.name
        else:
            return False

    def insert_value(self):
        if self.name in self.parent.form_params and self.parent.form_params[self.name]:
            d = datetime.datetime.fromtimestamp(int(self.parent.form_params[self.name]))
            return d
        else:
            return False

    def update_key(self):
        if self.name in self.parent.form_params:
            return self.name
        else:
            return False

    def update_value(self):
        if self.name in self.parent.form_params and self.parent.form_params[self.name]:
            d = datetime.datetime.fromtimestamp(int(self.parent.form_params[self.name]))
            return d
        else:
            return None