import momoko
import psycopg2
import tornado.gen


class AdminConfig:
    query = "SELECT * FROM api_admin_config_defaults"
    db = None
    configData = {
        'guest': {
            'user': {
                'show_columns': [
                    'name', 'surname', 'u_type'
                ],
                'show_fields': [
                    'name', 'surname', 'u_type', 'has_image'
                ]
            },
        },
        'admin': {
            'user': {
                'show_columns': [
                    'name', 'surname', 'u_type'
                ],
                'show_fields': [
                    'name', 'surname', 'u_type', 'has_image'
                ]
            },
            'contact': {
                'show_columns': [
                    'id', 'org', 'name', 'surname', 'phone_number', 'site', 'comment', 'status'
                ],
                'show_fields': [
                    'id', 'org', 'name', 'surname', 'phone_number', 'site', 'comment', 'status'
                ]
            },
            'call': {
                'show_columns': [
                    'id', 'time', 'duration', 'surname', 'result', 'phone_number', 'comment'
                ],
                'show_fields': [
                    'id', 'time', 'duration', 'surname', 'result', 'phone_number', 'comment'
                ]
            },
            'message': {
                'show_columns': [
                    'id', 'time', 'message', 'phone2', 'comment'
                ],
                'show_fields': [
                    'id', 'time', 'message', 'phone2', 'comment'
                ]
            }
        }
    }
    enum_query = 'SELECT pg_type.typname as k, pg_enum.enumlabel as v, pg_enum.enumsortorder as position FROM pg_type JOIN pg_enum ON pg_enum.enumtypid = pg_type.oid;'
    practice_categories = {}
    autocomplete_array = {
        'user': {
            'table': 'api_users',
            'label_columns': ['name', 'surname'],
            'key': 'id',
            'user': 'admin',
            'tree': False,
            'translate_postfix': False
        },
        'tags': {
            'table': 'tags',
            'label_columns': ['ua'],
            'key': 'id',
            'user': 'admin',
            'tree': False,
            'translate_postfix': True
        },
        'speakers': {
            'table': 'api_speakers',
            'label_columns': ['name'],
            'key': 'id',
            'user': 'admin',
            'tree': False,
            'translate_postfix': True
        }
    }
    lookup_array = {
        'old_ua_entries': {
            'table': 'import_ua_post',
            'get_columns': ['title', 'used'],
            'key': 'id',
            'search_columns': ['title', 'slug'],
            'get_values': {
                'title_ua': 'title',
                'text_ua': 'text'
            },
            'user': 'admin',
            'tree': False,
            'translate_postfix': False,
            'where': ['parent = 0']
        }
    }
    settings = {
        'facebook_api_key': '394620030712315',
        'facebook_secret': '11d142b1bb4033aa849d7b6ece17de8c',
        'linkedin_api_key': '75d5qtukrpux2o',  # user token f9a04917-5f61-4f2d-82d6-485f79929c36
        'linkedin_secret': 'AfWeRsGx71axsKGG',  # user secret cdf52e7e-36b6-4bc1-b0f6-5ca45c44b04d
        'vk_api_key': '4771821',  # '4683204',
        'vk_secret': 'w5612JgN90XZEkEn4Psq',  # 'pi5vDA3vs0ighDsEbz64',
        'interkassa_api_key': '54d745adbf4efcf560d90bf0',
        'interkassa_secret': '1EkTE7WhGpx3LrAj'
    }
    meta_options = {
        'interkassa-verification': '163c1fd20e5eb07cca2e95a312df1e30',
    }

    def __init__(self, db):
        self.db = db
        # to show in submenu
        self.init_lookups()
        """cursor = yield momoko.Op(self.db.execute, self.query, cursor_factory=psycopg2.extras.RealDictCursor)
        rows = cursor.fetchall()
        for row in rows:
            if row['user_type'] in self.configData:
                if row['realty_type'] in self.configData[row['user_type']]:
                    """

    def init_lookups(self):
        en_old_entries_lookup = {'table': 'import_en_post', 'get_values': {'title_en': 'title', 'text_en': 'text'}}
        pl_old_entries_lookup = {'table': 'import_pl_post', 'get_values': {'title_pl': 'title', 'text_pl': 'text'}}
        ru_old_entries_lookup = {'table': 'import_ru_post', 'get_values': {'title_ru': 'title', 'text_ru': 'text'}}
        self.lookup_array['old_en_entries'] = self.lookup_array['old_ua_entries'].copy()
        self.lookup_array['old_en_entries'].update(en_old_entries_lookup)
        self.lookup_array['old_pl_entries'] = self.lookup_array['old_ua_entries'].copy()
        self.lookup_array['old_pl_entries'].update(pl_old_entries_lookup)
        self.lookup_array['old_ru_entries'] = self.lookup_array['old_ua_entries'].copy()
        self.lookup_array['old_ru_entries'].update(ru_old_entries_lookup)

    def get_base_config(self):
        query = 'SELECT * FROM api_base_config'
        cursor = yield momoko.Op(self.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
        self.practice_categories = cursor.fetchall()