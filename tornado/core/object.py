import tornado.web
import momoko
import psycopg2
import math
import core.properties as p
# import core.config as config


class Object:
    db = None
    user = None
    parent = None
    code = ''
    code_plural = ''
    title = 'object'
    form_title = 'Edit object'
    table = ''
    lang = 'ua'
    search_params = {}
    form_params = {}
    show_columns = []
    show_fields = []
    page = 1
    page_size = 20
    max_per_page = 100
    min_per_page = 10
    res_count = 0
    rows_data = []
    row_data = {}
    form_data = {
        'inputs': {},
        'title': '',
        'message': '',
        'type': '',
        'action': ''
    }
    public_uri = ''
    admin_table_uri = ''
    columns = {}
    extend_columns = {}
    delete_columns = {}
    # parts = {}
    errors = []
    # select_parts = {}
    # save_part = {}
    user_class = None
    files = None
    enctype = ""
    item_id = 0
    img_dir = 'static/images/'
    file_column_types = ['Image', 'RequiredImage', 'Images', 'Document']
    public_list_columns = ' title_%s as title, text_%s as text, slug, id, has_image '
    properties_data = {}
    linked_objects_data = {}
    autocomplete_array = {}
    locales = None
    search_options = []
    sort_options = []
    page_uri_params = ''

    def __init__(self, db, user, config_data, search_params=None, form_params=None, user_class=None, files=None, lang='ua', locales=None):
        self.lang = lang
        self.db = db
        self.user = user
        self.files = files
        self.locales = locales
        self.columns.update(self.extend_columns)
        for k in self.delete_columns:
            del(self.columns[k])
        self.img_dir += self.code + '/'
        for key in self.columns:
            if self.columns[key]['type'] in self.file_column_types:
                self.enctype = 'multipart/form-data'
        # if not user or not user['u_type']:
        user = dict(u_type='admin')
        self.user_class = user_class
        self.show_columns = config_data.configData[user['u_type']][self.code]['show_columns']
        self.show_fields = config_data.configData[user['u_type']][self.code]['show_fields']
        self.autocomplete_array = config_data.autocomplete_array
        if not 'id' in self.show_columns:
            self.show_columns.insert(0, 'id')
        if search_params is not None:
            self.search_params = search_params.copy()
            self.iterate_properties('validate_search')
        if form_params is not None:
            self.form_params = form_params.copy()
            self.errors = []
            self.iterate_properties('validate_form')
        if 'order_by' not in self.search_params and 'edited' in self.columns:
            self.search_params['order_by'] = 'edited'
            self.search_params['order_type'] = 'DESC'
        if 'page_size' in self.search_params:
            self.page_size = int(self.search_params['page_size'])
        self.search_options = []
        self.sort_options = []
        for index in self.search_params:
            if self.search_params[index]:
                if index in ['order_by', 'order_type']:
                    p_str = index + '=' + self.search_params[index]
                    self.sort_options.append(p_str)
                elif index in self.show_columns:
                    p_str = index + '=' + self.search_params[index]
                    self.search_options.append(p_str)
        self.search_options = '&'.join(self.search_options)
        self.sort_options = '&'.join(self.sort_options)
        self.page_uri_params = self.search_options + '&' + self.sort_options
        self.admin_table_uri = '%s/%s/%s/?' % (lang, 'admin', self.code_plural)
        # self.load_properties_data()

    def set_pager(self, res_count):
        if res_count > 0:
            self.res_count = int(res_count)
        if 'page_size' in self.search_params and self.min_per_page < self.search_params['page_size'] < self.max_per_page:
            self.page_size = int(self.search_params['page_size'])
        last_page = math.ceil(self.res_count / self.page_size)
        if 'page' in self.search_params and 0 < int(self.search_params['page']) <= last_page:
            self.page = int(self.search_params['page'])

    def iterate_properties(self, func_name, items='all', arr=False, empty=False):
        result_str = ''
        result_dict = {}
        result_list = []
        if items == 'columns':
            l = self.show_columns
        elif items == 'fields':
            l = self.show_fields
        else:
            l = self.columns

        for index in l:
            if index in self.columns:
                cls = getattr(p, self.columns[index]['type'])
                prop = cls(index, self, self.columns[index])
                func = getattr(prop, func_name)
                result = func()
                if result or (empty and result == ''):
                    if not arr:
                        result_str = result_str + result
                    result_dict[index] = result
                    result_list.append(result)
        if arr == 'd':
            return result_dict
        elif arr == 'l':
            return result_list
        else:
            return result_str

    @tornado.gen.coroutine
    def load_properties_data(self):
        if not self.properties_data:
            for index in self.columns:
                cls = getattr(p, self.columns[index]['type'])
                prop = cls(index, self, self.columns[index])
                if prop.data_preload:
                    yield prop.load_data()

    @tornado.gen.coroutine
    def load_linked_objects_data(self):
        for index in self.show_fields:
            cls = getattr(p, self.columns[index]['type'])
            prop = cls(index, self, self.columns[index])
            if prop.load_objects:
                yield prop.load_objects()

    @tornado.gen.coroutine
    def async_iterate_fields(self, func_name, flag_name):
        for index in self.show_fields:
            cls = getattr(p, self.columns[index]['type'])
            prop = cls(index, self, self.columns[index])
            flag = getattr(prop, flag_name)
            if flag:
                func = getattr(prop, func_name)
                yield func()

    def insert(self, data):
        pass

    def form_inputs_html(self):
        return self.iterate_properties('html', 'fields' 'd')

    def table_search(self):
        return self.iterate_properties('th_search', 'columns')

    def table_sort(self):
        return self.iterate_properties('th_title', 'columns')

    def objects_count_query(self):
        joins = ' '.join(self.iterate_properties('join_table', 'columns', 'l'))
        if joins:
            tables = self.table + joins
        else:
            tables = self.table

        str_query = 'SELECT COUNT(*) FROM %s' % (tables,)

        where_str = self.iterate_properties('where_sql', 'columns', 'l')
        if where_str:
            where_str = ' AND '.join(where_str)
            str_query = str_query + ' WHERE ' + where_str
        return str_query

    def select_query(self, select=None, where=None):
        if not select:
            select = self.iterate_properties('select_sql', 'columns', 'l')
            select = ', '.join(select)
        joins = ' '.join(self.iterate_properties('join_table', 'columns', 'l'))
        if joins:
            tables = self.table + joins
        else:
            tables = self.table
        str_query = 'SELECT %s FROM %s' % (select, tables)
        where_str = self.iterate_properties('where_sql', 'columns', 'l')
        if where_str:
            str_query = str_query + ' WHERE ' + ' AND '.join(where_str)
        rows_sort = self.iterate_properties('get_sort', 'columns')
        if rows_sort:
            str_query += ' ORDER BY ' + rows_sort
        page_sql = ' LIMIT %s OFFSET %s' % (self.page_size, (self.page - 1) * self.page_size)
        str_query += page_sql
        return str_query

    def get_rows(self):
        table_cells = []
        for row in self.rows_data:
            self.row_data = row
            if 'id' in row:
                self.item_id = row['id']
            td = self.iterate_properties('td', 'columns')
            tr = '<tr data-url="/{{ handler.locale.code }}/admin/%s/%s">%s</tr>' % (self.code, row['id'], td)
            table_cells.append(tr)
        return table_cells

    @tornado.gen.coroutine
    def results_count(self):
        cursor = yield momoko.Op(self.db.execute, self.objects_count_query())
        results_count = cursor.fetchone()[0]
        return results_count

    @tornado.gen.coroutine
    def results(self, table_rows=False, public=False, lang=None):
        if public:
            select = self.public_list_columns % (lang, lang)
            select = self.select_query(select)
        else:
            select = self.select_query()
        cursor = yield momoko.Op(self.db.execute, select, cursor_factory=psycopg2.extras.RealDictCursor)
        rows = cursor.fetchall()
        self.rows_data = rows
        if table_rows:
            return self.get_rows()
        return rows

    def get_object_link(self, id):
        if id == 'add':
            return '/{{ handler.locale.code }}/admin/%s/add' % (self.code,)
        else:
            return '/{{ handler.locale.code }}/admin/%s/%s' % (self.code, id)

    @tornado.gen.coroutine
    def get_form_data(self, id):
        if id.isnumeric():
            main_data_query = "SELECT * FROM %s WHERE id=%s" % (self.table, id)
            cursor = yield momoko.Op(self.db.execute, main_data_query, cursor_factory=psycopg2.extras.RealDictCursor)
            self.row_data = cursor.fetchone()
            if self.row_data:
                self.item_id = id
                # yield self.load_linked_objects_data()
                self.form_data['inputs'] = self.iterate_properties('html', 'fields', 'l')
            else:
                return False
        elif id == 'add':
            self.row_data = {}
            self.form_data['inputs'] = self.iterate_properties('html', 'fields', 'l')

        self.form_data['action'] = self.get_object_link(id)
        self.form_data['type'] = self.code
        return self.form_data

    @tornado.gen.coroutine
    def save_item(self, id):
        if id.isnumeric():
            query = "UPDATE %s SET %s WHERE id=%s"
            update_keys = self.iterate_properties('update_key', 'all', 'l')
            update_keys_str = '=%s, '.join(update_keys) + '=%s'
            update_values = self.iterate_properties('update_value', 'all', 'l', True)
            query %= (self.table, update_keys_str, id)
            try:
                yield momoko.Op(self.db.execute, query, tuple(update_values))
                self.item_id = id
                # self.iterate_properties('after_save')
                yield self.async_iterate_fields('after_save', '_after_save')
                return True
            except Exception as e:
                self.errors.append(e.pgerror)
                print(e)
                return False
        else:
            insert_keys = self.iterate_properties('insert_key', 'all', 'l')
            values_str = '%s, ' * (len(insert_keys)-1) + '%s'
            keys_str = ', '.join(insert_keys)
            insert_values = self.iterate_properties('insert_value', 'all', 'l', True)
            query = "INSERT INTO %s (%s) VALUES (%s) RETURNING id" % (self.table, keys_str, values_str)
            try:
                cursor = yield momoko.Op(self.db.execute, query, tuple(insert_values))
                id = cursor.fetchone()[0]
                self.item_id = id
                return id
            except Exception as e:
                self.errors.append(e.pgerror)
                print(e)
                return False