separator = ": "


def add_label(html_str, label):
    if label:
        label_html = '<label>%s%s</label>' % (label, separator)
        html_str = label_html + html_str
    return html_str


def add_parent_tag(html_str, tag, classname=""):
    if classname:
        classname = 'class="%s"' % (classname,)
    if tag:
        html_str = '<%s %s>%s</%s>' % (tag, classname, html_str, tag)
    return html_str


def field(name, inp_type="text", value="", label="", placeholder="", tag="", required='', disabled='', classname=''):
    if required:
        required = 'required="required"'
    if disabled:
        disabled = 'disabled="disabled"'
    if classname:
        classname = 'class="%s"' % (classname,)
    html_str = '<input name="%s" type="%s" value="%s" placeholder="{{ _("%s") }}" %s %s %s />' % (name, inp_type, value, placeholder, required, disabled, classname)
    html_str = add_label(html_str, label)
    html_str = add_parent_tag(html_str, tag)
    return html_str


def select(name="", values={}, selected=None, label="", tag="", required=None):
    if required:
        required = 'required="required"'
    else:
        required = ''
    options = ''
    for raw in values:
        if selected == raw['id']:
            selected_attr = 'selected'
        else:
            selected_attr = ''
        option = '<option value="%s" %s>%s</option>' % (raw['id'], selected_attr, raw['label'])
        options += option
    html_str = '<select name="%s" %s>%s</select>' % (name, required, options)
    html_str = add_label(html_str, label)
    html_str = add_parent_tag(html_str, tag)
    return html_str


def checkbox(name, checked, label="", tag=""):
    value = checked
    if checked:
        checked = 'checked="checked"'
    else:
        checked = ''
    html_str = '<input type="checkbox" name="%s" value="%s" %s />' % (name, value, checked)
    html_str = add_label(html_str, label)
    html_str = add_parent_tag(html_str, tag)
    return html_str


def textarea(name, value="", label="", placeholder="", tag="", classname=""):
    classattr = ''
    if classname:
        classattr = 'class="%s"' % (classname,)
    html_str = '<textarea %s name="%s" placeholder="{{ _("%s") }}">%s</textarea>' % (classattr, name, placeholder, value)
    html_str = add_label(html_str, label)
    html_str = add_parent_tag(html_str, tag)
    return html_str


def image(name, file_name=None, label="", placeholder="", tag="", classname="", required=False):
    if required:
        required = 'required="required"'
    else:
        required = ''
    if file_name:
        img = '<img src="%s"/>' % (file_name,)
    else:
        img = ''
    html_str = '<input type="file" name="%s" placeholder="{{ _("%s") }}" %s />' % (name, placeholder, required)
    html_str += img
    html_str = add_label(html_str, label)
    html_str = add_parent_tag(html_str, tag)
    return html_str


def ac(ac_type, name, href, val_key='', val_label='', parent='', child='', label=None, required='', classname='_ac'):
    if parent:
        parent = 'data-parent="%s"' % (parent,)
    if child:
        child = 'data-child="%s"' % (child,)
    if required:
        required = 'required="required"'
    href = 'data-href="%s"' % (href,)
    html_str = '<input type="hidden" name="%s" value="%s" %s />' % (name, val_key, required)
    html_str += '<input type="text" name="%s_ac" value="%s" %s %s data-type="%s" class="%s" autocomplete="off" %s %s>' % (name, val_label, parent, child, ac_type, classname, required, href)
    html_str = add_label(html_str, label)
    html_str += '<ul class="_acList" style="display: none;"></ul>'
    html_str = add_parent_tag(html_str, 'div', 'ac_box')
    return html_str


def lookup(l_type, name, value, href, label=None, required=''):
    if required:
        required = 'required="required"'
    href = 'data-href="%s"' % (href,)
    lookup_val_name = name + '_' + l_type
    html_str = '<input type="hidden" name="%s" %s />' % (lookup_val_name, required)
    html_str += '<input type="text" name="%s" value="%s" data-type="%s" class="_lu" autocomplete="off" %s %s>' % (name, value, l_type, required, href)
    html_str = add_label(html_str, label)
    html_str += '<table class="_luTable" style="display: none;"></table>'
    html_str = add_parent_tag(html_str, 'div', 'lu_box')
    return html_str


def tags(inp, name, values=[], all_values=[], data={}, label='', classname='tag_box', lang=None):
    values_str = ''
    for obj_id in values:
        for raw in all_values:
            if str(raw['id']) == str(obj_id):
                values_str += '<li><input type="hidden" name="%s[]" value="%s" />%s<i class="_remove_parent fa fa-times"></i></li>' % (name, raw['id'], raw['label'])
    html_str = '<ul>%s</ul><div>%s</div>' % (values_str, inp)
    html_str = '<fieldset class="_tags %s"><legend>%s</legend>%s</fieldset>' % (classname, label, html_str)
    return html_str