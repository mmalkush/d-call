CREATE TABLE contacts (
  id serial PRIMARY KEY,
  org varchar(250),
  name varchar(250),
  surname varchar(250),
  phone_number varchar(250),
  site varchar(250),
  found_on varchar(250),
  status integer,
--   user_id integer REFERENCES api_users(id),
  comment varchar(250)
);