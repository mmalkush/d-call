CREATE TABLE api_users (
  id serial PRIMARY KEY,
  name varchar(100),
  surname varchar(100),
  email varchar(100) UNIQUE,
  reg_id varchar(250) UNIQUE,
  auth_failed smallint,
  has_image boolean
);