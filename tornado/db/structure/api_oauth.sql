CREATE TABLE api_oauth (
  id serial PRIMARY KEY,
  user_id integer REFERENCES api_users(id),
  social_network varchar(100),
  soc_id varchar(250),
  soc_key varchar(250),
  UNIQUE (social_network, soc_id)
);