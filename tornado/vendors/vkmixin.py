from tornado import auth
import functools
import urllib.parse as urllib_parse


class VKMixin(auth.OAuth2Mixin):
    _OAUTH_AUTHORIZE_URL = "https://oauth.vk.com/authorize"
    _OAUTH_ACCESS_TOKEN_URL = "https://oauth.vk.com/access_token"
    _OAUTH_BASE_API_URL = "https://api.vk.com/method"
    _OAUTH_NO_CALLBACKS = False