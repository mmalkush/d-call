import tornado.web
import handlers.base
import core.pubs
# import core.cats
import core.user
import os.path
import momoko
from time import localtime


class DashboardHandler(handlers.base.BaseHandler):
    @tornado.web.authenticated
    def get(self, lang=None):
        self.render("admin/dashboard.html")


class ContactsHandler(handlers.base.BaseHandler):
    cls = 'contact'

    def get_object_class(self):
        # mdl = getattr(core.pubs, self.cls)
        cls = getattr(core.pubs, self.cls[0].upper()+self.cls[1:])
        return cls

    @tornado.gen.coroutine
    @tornado.web.authenticated
    def get(self, lang=None):
        user = self.get_current_user()
        params = self.get_params()

        cls = self.get_object_class()
        obj = cls(self.db, user, self.admin_config, params, user_class=self.user, locales=self.supported_locales)

        th_sort = self.gen_template(obj.table_sort())
        th_search = self.gen_template(obj.table_search())
        results_count = yield obj.results_count()
        obj.set_pager(results_count)
        rows = yield obj.results(True)
        rows_html = []
        for row in rows:
            rows_html.append(self.gen_template(row))

        if self.is_ajax():
            html = self.render_string("admin/posts_ajax.html", realty_type=obj.code, realty_title='flats_for_sale', th_sort=th_sort, th_search=th_search, page=obj.page, page_size=obj.page_size,
                                      results_count=results_count, rows=rows_html, title=obj.title, subtitle=obj.code_plural, ajax=True, table_uri=obj.admin_table_uri)
            self.json_response(data={'html': html.decode('utf-8'), 'element': '#content'})
        else:
            self.render("admin/posts_full.html", realty_type=obj.code, realty_title='flats_for_sale', th_sort=th_sort, th_search=th_search, page=obj.page, page_size=obj.page_size,
                        results_count=results_count, rows=rows_html, title=obj.title, subtitle=obj.code_plural, ajax=False, table_uri=obj.admin_table_uri)
        self.finish()


class ContactHandler(ContactsHandler):
    cls = 'contact'
    
    @tornado.gen.coroutine
    @tornado.web.authenticated
    def get(self, lang, id=None):
        user = self.get_current_user()

        cls = self.get_object_class()
        locale = self.get_user_locale()
        obj = cls(self.db, user, self.admin_config, lang=locale, locales=self.supported_locales)

        form_data = yield obj.get_form_data(id)
        if not form_data:
            raise tornado.web.HTTPError(404)
        for index, item in enumerate(form_data['inputs']):
            form_data['inputs'][index] = self.gen_template(item)
        form_data['action'] = self.gen_template(form_data['action'])

        if os.path.isfile("templates/modules/forms/" + self.cls + ".html"):
            t = "modules/forms/" + self.cls + ".html"
        else:
            t = "modules/forms/base_form.html"
        html = self.render_string(t, inputs=form_data["inputs"], message=form_data["message"], form_class=form_data["type"], action=form_data["action"],
                                  title=obj.form_title, enctype=obj.enctype)
        self.json_response(data={'html': html.decode('utf-8'), 'element': '#popup'})
        self.finish()

    @tornado.gen.coroutine
    @tornado.web.authenticated
    def post(self, lang=None, id=None):
        user = self.get_current_user()
        params = self.get_params()
        if len(params) == 0:
            return self.delete(id)
        files = self.request.files

        cls = self.get_object_class()
        obj = cls(self.db, user, self.admin_config, None, params, files=files)
        if len(obj.errors) > 0:
            self.json_response('error', 'please check all fields', {'formErrors': {'email': 'wrong email'}})
        else:
            result = yield obj.save_item(id)
            if result:
                ajaxlink = '/%s/admin/%s' % (lang, obj.code_plural)
                jscode = 'ajax("' + ajaxlink + '")'
                if id.isnumeric():
                    self.json_response('ok', obj.code + ' saved successfully', {}, jscode)
                else:
                    action = self.gen_template(obj.get_object_link(result)).decode('utf-8')
                    self.json_response('ok', obj.code + ' saved successfully', {'action': action}, jscode)
            else:
                self.json_response('error', 'unexpected error')
        self.finish()

    def delete(self, obj_id):
        pass


class CallsHandler(ContactsHandler):
    cls = 'call'


class CallHandler(ContactHandler):
    cls = 'call'


class MessagesHandler(ContactsHandler):
    cls = 'message'


class MessageHandler(ContactHandler):
    cls = 'message'


class UsersHandler(ContactsHandler):
    cls = 'user'

    def get_object_class(self):
        cls = getattr(core.user, self.cls[0].upper()+self.cls[1:])
        return cls


class UserHandler(ContactHandler):
    cls = 'user'

    def get_object_class(self):
        cls = getattr(core.user, self.cls[0].upper()+self.cls[1:])
        return cls


class AddContactHandler(handlers.base.BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        phone = "zzz"
        query = "INSERT INTO contacts (phone) VALUES (%s)"
        yield momoko.Op(self.db.execute, query, (phone))
        return


import json
from gcm import GCM


class AddCallHandler(handlers.base.BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        duration = self.get_argument("duration", "120")
        is_coming = self.get_argument("isComing", True)
        date = self.get_argument("time", True)
        phone = self.get_argument("phoneNumber", "0631021012")
        query = "INSERT INTO calls (duration, time, is_coming, phone_number) VALUES (%s, %s, %s, %s)"
        yield momoko.Op(self.db.execute, query, (duration, localtime(int(date)), is_coming, phone))
        return


class AddSMSHandler(handlers.base.BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        date = self.get_argument("time", True)
        phone = self.get_argument("phone", "0631021012")
        message = self.get_argument("body", "")
        query = "INSERT INTO messages (time, message, phone2) VALUES (%s, %s, %s)"
        yield momoko.Op(self.db.execute, query, (localtime(int(date)), message, phone))
        return


class SiteCallHandler(handlers.base.BaseHandler):
    @tornado.gen.coroutine
    def post(self):
        number = self.get_argument("number", "0631021012")
        site = self.get_argument("site", "http://laptopsale.com.ua/")
        # query = "INSERT INTO calls (site, comment) VALUES (%s, %s)"
        # yield momoko.Op(self.db.execute, query, (number, site))
        gcm = GCM('AIzaSyAAl32yCktq8X_SlsJZhUjtU1gToZubJXA')
        reg_id = 'APA91bFbChBAVNknfunOiPRYFDWa87kj1t8Tl6ZIT0kCWqehSQMkkW9784OiWQne7w-EeFgSmyXMiO1W8sGy9f52m3EWcIVnj-PCQ48UBWeSmXWE7bFUXtGTRQQ13d9IabHZZLE0ZOHfSG9BCAOS1a6_mFGno-PuSQ'
        json_params = {"number": str(number), "site": str(site)}
        gcm.plaintext_request(registration_id=reg_id, data=json_params, time_to_live=0)
        return
        yield
