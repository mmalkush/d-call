import tornado.web
import tornado.gen
import tornado.auth
import momoko
import psycopg2
import tornado.escape
from tornado import template
import core.pubs
from vendors.vkmixin import VKMixin
from vendors.linkedinmixin import LinkedInMixin
from random import randint
from tornado import httpclient
# import core.pubs
# import core.cats


class BaseHandler(tornado.web.RequestHandler):
    supported_locales = None
    locale_titles = {'ua': 'укр', 'en': 'eng', 'pl': 'pol', 'ru': 'рус'}

    @property
    def db(self):
        return self.application.db

    @property
    def r(self):
        return self.application.r

    @property
    def captcha(self):
        return self.application.captcha

    @property
    def system_font(self):
        return self.application.system_font

    @property
    def user(self):
        return self.application.user

    @property
    def admin_config(self):
        return self.application.adminConfig

    def json_response(self, status='ok', message='', data={}, code=''):
        self.write(dict(status=status, message=message, data=data, code=code))

    def get_current_user(self):
        user_json = self.get_secure_cookie("user")
        if user_json:
            return tornado.escape.json_decode(user_json)
        else:
            return None

    def get_user_locale(self):
        tornado.locale.set_default_locale("ua")
        self.supported_locales = tornado.locale.get_supported_locales()
        # path_args = self.request.path[1:].split('/')
        # if path_args[0] in self.supported_locales:
        #     return path_args[0]
        # else:
        #     return path_args[1]
        if len(self.path_args) > 0 and self.path_args[0] in self.supported_locales:
            return tornado.locale.get(self.path_args[0])
        else:
            return tornado.locale.get("ua")

    def gen_template(self, data):
        return template.Template(data).generate(**dict(_=self.locale.translate, handler=self))

    def get_param(self, key):
        if key[-2:] == '[]':
            result = self.get_arguments(key, [])
        else:
            result = self.get_argument(key, '')
        return result

    def get_params(self):
        params = {}
        for k in self.request.arguments:
            params[k] = self.get_param(k)
        return params

    def write_error(self, status_code, **kwargs):
        errors = [404, 405]
        if not status_code in errors:
            status_code = errors[0]
        t = 'errors/%s.html' % (status_code,)
        self.render(t)

    def is_ajax(self):
        if 'ajax' in self.request.uri:
            return True
        else:
            return False


class ErrorHandler(BaseHandler):
    pass


class HomeHandler(BaseHandler):
    def get(self, lang=None):
        self.render("public/welcome.html")


class LoginHandler(BaseHandler):
    def get(self, lang=None):
        self.render("admin/login.html")

    @tornado.gen.coroutine
    def post(self, lang=None):
        email = self.get_argument("email", "")
        password = self.get_argument("password", "")
        cursor = yield momoko.Op(self.db.execute, "SELECT 1", cursor_factory=psycopg2.extras.RealDictCursor)
        # cursor = yield momoko.Op(self.db.execute, self.user.queries['getByEmail'], (email,), cursor_factory=psycopg2.extras.RealDictCursor)
        # user = cursor.fetchone()
        user = {
            'id': 1,
            'name': 'superuser',
            'password': 'password',
            'u_type': 'admin'
        }
        if user:
            if user['password'] == password or True:
                self.set_current_user(user)
                self.json_response(code='location.reload();')
            else:
                self.json_response('error', 'wrong password', {'formErrors': {'password': 'wrong password'}})
        else:
            self.json_response('error', 'wrong email', {'formErrors': {'email': 'wrong email'}})

        self.finish()

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", tornado.escape.json_encode(user))
        else:
            self.clear_cookie("user")


class LogoutHandler(BaseHandler):
    def get(self, lang=None):
        self.clear_cookie("user")
        self.redirect("/")


class RegisterHandler(BaseHandler):
    @tornado.gen.coroutine
    def get(self, lang=None):
        cursor = yield momoko.Op(self.db.execute, 'SELECT COUNT(*) FROM ' + self.user.table)
        self.write('Results2: %r' % (cursor.fetchall()))
        self.finish()

    def post(self, lang=None):
        email = self.get_argument("email", "")
        password = self.get_argument("password", "")
        self.user.reg(email, password)


class StaticPage(BaseHandler):
    code = "about"
    table = "api_static_pages"
    query = "SELECT * FROM %s WHERE code = '%s'"

    @tornado.gen.coroutine
    def get(self, lang):
        query = self.query % (self.table, self.code)
        cursor = yield momoko.Op(self.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
        data = cursor.fetchone()
        title = data["title_" + lang]
        html = data["html_" + lang]
        self.render("public/" + self.code + ".html", title=title, html=html)


class ObjectHandler(BaseHandler):
    code = "post"
    table = "api_posts"

    def get(self, lang=None):
        self.render("public/team.html")


class PubHandler(BaseHandler):
    code = "post"
    table = "api_posts"
    query = "SELECT * FROM {table} WHERE slug = %s"

    @tornado.gen.coroutine
    def get(self, lang, slug):
        query = self.query.replace('{table}', self.table)
        cursor = yield momoko.Op(self.db.execute, query, (slug,), cursor_factory=psycopg2.extras.RealDictCursor)
        data = cursor.fetchone()
        data['text'] = data['text_' + lang]
        data['title'] = data['title_' + lang]
        self.render("public/" + self.code + ".html", data=data)


class PubListHandler(BaseHandler):
    cls = 'post'
    code = "posts"
    table = "api_posts"
    query = "SELECT id, slug, created, clicks, title_%s as title, text_%s as text, has_image FROM %s ORDER BY %s LIMIT %s OFFSET %s"
    fresh_order_by = 'created'
    popular_order_by = 'clicks'
    limit = 10

    @tornado.gen.coroutine
    def get(self, lang, page=1):
        params = {'page': page, 'checked': True, 'hidden': False, 'order_by': self.fresh_order_by, 'page_size': 10}
        cls = getattr(core.pubs, self.cls.capitalize())
        obj = cls(self.db, None, self.admin_config, params, user_class=self.user)

        results_count = yield obj.results_count()
        obj.set_pager(results_count)
        rows = yield obj.results(False, True, lang)

        if self.is_ajax():
            html = self.render_string("public/" + self.code + "_ajax.html", page=obj.page, page_size=obj.page_size, results_count=results_count, rows=rows, ajax=True, image_prefix=self.code)
            self.json_response(data={'html': html.decode('utf-8'), 'element': '#content'})
            self.finish()
        else:
            self.render("public/" + self.code + "_full.html", page=obj.page, page_size=obj.page_size, results_count=results_count, rows=rows, ajax=False, image_prefix=self.code)


class SearchHandler(BaseHandler):
    code = "search"

    def get(self, lang=None):
        self.render("public/team.html")


class FBLoginHandler(LoginHandler, tornado.auth.FacebookGraphMixin):
    @tornado.gen.coroutine
    def get(self):
        redirect_uri = 'http://api.findhome.com.ua/fb_login'
        if self.get_argument("code", False):
            user = yield self.get_authenticated_user(
                redirect_uri=redirect_uri,
                client_id=self.admin_config.settings["facebook_api_key"],
                client_secret=self.admin_config.settings["facebook_secret"],
                code=self.get_argument("code"))
            yield self.user.oauth_login(self, 'fb', user['id'], user['access_token'].decode('utf-8'), user['first_name'], user['last_name'], user['picture'])
            self.write('<script>window.opener.location.reload(false); window.close();</script>')
        else:
            yield self.authorize_redirect(
                redirect_uri=redirect_uri,
                client_id=self.admin_config.settings["facebook_api_key"],
                extra_params={"scope": "read_stream,offline_access"})


class VKLoginHandler(LoginHandler, tornado.auth.OAuth2Mixin):
    _OAUTH_AUTHORIZE_URL = "https://oauth.vk.com/authorize"
    _OAUTH_ACCESS_TOKEN_URL = "https://oauth.vk.com/access_token?"
    _OAUTH_BASE_API_URL = "https://api.vk.com/method/"
    _OAUTH_NO_CALLBACKS = False

    @tornado.gen.coroutine
    def get(self):
        redirect_uri = 'http://api.findhome.com.ua/vk_login'
        if self.get_argument("code", False):
            http = httpclient.AsyncHTTPClient()
            request = self._OAUTH_ACCESS_TOKEN_URL + 'client_id=%s&client_secret=%s&code=%s&redirect_uri=%s' % (self.admin_config.settings["vk_api_key"], self.admin_config.settings["vk_secret"], self.get_argument("code"), redirect_uri)
            response = yield http.fetch(request)
            json = tornado.auth.escape.json_decode(response.body)
            user = {'access_token': json['access_token'], 'id': json['user_id']}
            request = self._OAUTH_BASE_API_URL + 'users.get?user_id=%s&access_token=%s' % (user['id'], user['access_token'])
            response = yield http.fetch(request)
            json = tornado.auth.escape.json_decode(response.body)
            user['first_name'] = json['response'][0]['first_name']
            user['last_name'] = json['response'][0]['last_name']
            yield self.user.oauth_login(self, 'vk', user['id'], user['access_token'], user['first_name'], user['last_name'])
            self.write('<script>window.opener.location.reload(false); window.close();</script>')
        else:
            yield self.authorize_redirect(
                redirect_uri=redirect_uri,
                client_id=self.admin_config.settings["vk_api_key"],
                extra_params={"scope": "read_stream,offline_access"})


class LILoginHandler(LoginHandler, LinkedInMixin):
    @tornado.gen.coroutine
    def get(self):
        code = self.get_argument("code", None)
        redirect_uri = 'http://api.findhome.com.ua/li_login'

        if not code:
            # Generate a random state
            r = str(randint(999999, 9999999))
            state = r

            self.set_secure_cookie("linkedin_state", state)

            yield self.authorize_redirect(
                redirect_uri=redirect_uri,
                client_id=self.admin_config.settings["linkedin_api_key"],
                extra_params={
                    "response_type": "code",
                    "state": state,
                    "scope": "r_basicprofile r_emailaddress"
                }
            )

            return

        # Validate the state
        # if self.get_argument("state", None) != self.get_secure_cookie("linkedin_state"):
        #     raise tornado.web.HTTPError(400, "Invalid state")

        user = yield self.get_authenticated_user(
            redirect_uri=redirect_uri,
            client_id=self.admin_config.settings["linkedin_api_key"],
            client_secret=self.admin_config.settings["linkedin_secret"],
            code=code,
            extra_fields=["formatted-name", "email-address"]
        )
        yield self.user.oauth_login(self, 'ln', user['id'], user['access_token'], user['formattedName'], '', email=user['emailAddress'])
        self.write('<script>window.opener.location.reload(false); window.close();</script>')

        if not user:
            raise tornado.web.HTTPError(400, "LinkedIn authentication failed")