import tornado
from handlers.base import *


class ContactsHandler(BaseHandler):
    code = "contacts"
    table = "api_feedback"
    insert_for_guest = "INSERT INTO api_feedback (name, email, phone, message) VALUES (%s, %s, %s, %s)"
    insert_for_user = "INSERT INTO api_feedback (message, user_id) VALUES (%s, %s)"

    def get(self, lang):
        captcha = self.gen_template(self.captcha.render())
        self.render("public/contacts.html", captcha=captcha)

    @tornado.gen.coroutine
    def post(self, lang):
        user = self.get_current_user()
        if user:
            message = self.get_argument("message", "")
            yield momoko.Op(self.db.execute, self.insert_for_user, (message, user["id"]))
        else:
            message = self.get_argument("message", "")
            phone = self.get_argument("phone", "")
            name = self.get_argument("name", "")
            email = self.get_argument("email", "")
            captcha = self.get_argument(self.captcha.captcha_name, "")
            captcha_hash = self.get_argument(self.captcha.captcha_hash_name, "")
            if self.captcha.check([captcha, captcha_hash]):
                yield momoko.Op(self.db.execute, self.insert_for_guest, (name, email, phone, message))
            else:
                new_captcha = self.gen_template(self.captcha.render())
                self.json_response('error', 'wrong captcha', {'html': new_captcha, 'element': '#contact_captcha_box'})

        self.json_response('ok', 'Your request successfully sent!', {'resetForm': True})
        self.finish()


class TeamHandler(ObjectHandler):
    code = "team"


class TeamMemberHandler(ObjectHandler):
    code = "team_member"


class PostsHandler(PubListHandler):
    pass


class PostHandler(PubHandler):
    pass


class EntriesHandler(PubListHandler):
    cls = 'entry'
    code = "entries"
    table = 'api_entries'


class EntryHandler(PubHandler):
    cls = 'entry'
    code = "entry"
    table = 'api_entries'


class EventsHandler(ObjectHandler):
    cls = 'event'
    code = "events"
    table = 'api_entries'


class EventHandler(ObjectHandler):
    cls = 'event'
    code = "event"
    table = 'api_entries'


class ReviewsHandler(ObjectHandler):
    cls = 'review'
    code = "reviews"
    table = 'api_reviews'


class ReviewHandler(ObjectHandler):
    cls = 'review'
    code = "review"
    table = 'api_reviews'


class BusinessesHandler(PubListHandler):
    code = "team_member"


class BusinessHandler(ObjectHandler):
    code = "team_member"


class CommentsHandler(ObjectHandler):
    code = "team_member"


class CommentHandler(ObjectHandler):
    code = "team_member"


class CommentHandler(ObjectHandler):
    code = "team_member"


class EventPayHandler(ObjectHandler):
    code = "pay"


class PracticesHandler(PubListHandler):
    cls = 'practice'
    code = "practice"
    table = 'api_practice'

    @tornado.gen.coroutine
    def get(self, lang, cat, page=1):
        params = {'page': page, 'checked': True, 'hidden': False, 'order_by': self.fresh_order_by, 'page_size': 10}
        cls = getattr(core.pubs, self.cls.capitalize())
        obj = cls(self.db, None, self.admin_config, params, user_class=self.user)

        results_count = yield obj.results_count()
        obj.set_pager(results_count)
        rows = yield obj.results(False, True, lang)

        if self.is_ajax():
            html = self.render_string("public/" + self.code + "_ajax.html", page=obj.page, page_size=obj.page_size, results_count=results_count, rows=rows, ajax=True, image_prefix=self.code)
            self.json_response(data={'html': html.decode('utf-8'), 'element': '#content'})
            self.finish()
        else:
            self.render("public/" + self.code + "_full.html", page=obj.page, page_size=obj.page_size, results_count=results_count, rows=rows, ajax=False, image_prefix=self.code)


class PracticeHandler(PubHandler):
    cls = 'practice'
    code = "practice"
    table = 'api_practice'