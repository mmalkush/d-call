from handlers.base import BaseHandler
import tornado.web
import psycopg2
import momoko


class AutoCompleteHandler(BaseHandler):
    query = "SELECT {key} as id, {label} as label FROM {table} WHERE {where} LIMIT 5"

    @tornado.gen.coroutine
    def get(self, lang):
        # TODO: check user rights
        ac_type = self.get_argument('type')
        ac_value = self.get_argument('value')

        if ac_type in self.admin_config.autocomplete_array:
            data = self.admin_config.autocomplete_array[ac_type]
            query = self.query.replace('{table}', data['table'])
            query = query.replace('{key}', data['key'])
            query = query.replace('{label}', " || ' ' || ".join(data['label_columns']))
            params = []
            label_columns = []
            for index, item in enumerate(data['label_columns']):
                label_columns.append('lower(' + data['label_columns'][index] + ") LIKE %s")
                params.append("lower('%%" + ac_value + "%%')")
            where_sql = ' OR '.join(label_columns)
            query = query.replace('{where}', where_sql)
            query = query % tuple(params)
            # params = tuple(params)
            cursor = yield momoko.Op(self.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
            # cursor = yield momoko.Op(self.db.execute, query, params, cursor_factory=psycopg2.extras.RealDictCursor)
            rows = cursor.fetchall()
            html = ''
            first = True
            for row in rows:
                if first:
                    classname = 'class="active"'
                    first = False
                else:
                    classname = ''
                html += '<li data-id="%s" data-name="%s" %s>%s' % (row['id'], row['label'], classname, row['label'])
            self.json_response(data={'html': html})
        # self.json_response(data={'html': query, 'element': '#content'})
        self.finish()


class LookupHandler(AutoCompleteHandler):
    query = "SELECT {key} as id, {columns} FROM {table} WHERE {where} LIMIT 5"

    @tornado.gen.coroutine
    def get(self, lang):
        # TODO: check user rights
        lu_type = self.get_argument('type')
        lu_value = self.get_argument('value')

        if lu_type in self.admin_config.lookup_array:
            data = self.admin_config.lookup_array[lu_type]
            query = self.query.replace('{table}', data['table'])
            query = query.replace('{key}', data['key'])
            query = query.replace('{columns}', ", ".join(data['get_columns']))
            params = []
            search_columns = []
            for index, item in enumerate(data['search_columns']):
                search_columns.append('lower(' + data['search_columns'][index] + ") LIKE %s")
                params.append("'%%" + lu_value.lower() + "%%'")
            where_sql = ' OR '.join(search_columns)
            if data['where']:
                config_where = ' AND '.join(data['where'])
                where_sql = '%s AND (%s)' % (config_where, where_sql)
            query = query.replace('{where}', where_sql)
            query = query % tuple(params)
            cursor = yield momoko.Op(self.db.execute, query, cursor_factory=psycopg2.extras.RealDictCursor)
            rows = cursor.fetchall()
            html = ''
            first = True
            for row in rows:
                if first:
                    classname = 'class="active"'
                    first = False
                else:
                    classname = ''
                td_str = ''
                for column in data['get_columns']:
                    val = '' if row[column] is None else row[column]
                    td_str += '<td>%s</td>' % val
                href = '/%s/lu/%s/%s/' % (lang, lu_type, row['id'])
                html += '<tr data-href="%s" data-id="%s" %s>%s</tr>' % (href, row['id'], classname, td_str)
            self.json_response(data={'html': html})
        self.finish()


class LookupAutoFill(BaseHandler):
    query = "SELECT {columns} FROM {table} WHERE {key}=%s"

    @tornado.gen.coroutine
    def get(self, lang, lu_type, id):
        # TODO: check user rights

        if lu_type in self.admin_config.lookup_array:
            data = self.admin_config.lookup_array[lu_type]
            query = self.query.replace('{table}', data['table'])
            query = query.replace('{key}', data['key'])
            columns = data['get_values'].values()
            columns = ", ".join(columns)
            query = query.replace('{columns}', columns)
            cursor = yield momoko.Op(self.db.execute, query, (id,), cursor_factory=psycopg2.extras.RealDictCursor)
            row = cursor.fetchone()
            form_values = {}
            for index in data['get_values']:
                form_values[index] = row[data['get_values'][index]]
            self.json_response(data={'formValues': form_values})
        self.finish()