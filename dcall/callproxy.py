import asyncio
import jinja2

from aiohttp import web
from gcm import GCM

jenv = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'))
gcm = GCM('AIzaSyAAl32yCktq8X_SlsJZhUjtU1gToZubJXA')
reg_id = 'APA91bFbChBAVNknfunOiPRYFDWa87kj1t8Tl6ZIT0kCWqehSQMkkW9784OiWQne7w-EeFgSmyXMiO1W8sGy9f52m3EWcIVnj-PCQ48UBWeSmXWE7bFUXtGTRQQ13d9IabHZZLE0ZOHfSG9BCAOS1a6_mFGno-PuSQ'

@asyncio.coroutine
def call_handler(request):
    print('method = {!r}; path = {!r}; version = {!r}'.format(
        request.method, request.path, request.version))
    post_params = yield from request.json()
    print(post_params)
    # send event to device
    # response = gcm.json_request(registration_id=reg_id, data=post_params)
    gcm.plaintext_request(registration_id=reg_id, data=post_params,
        time_to_live=0)
    return web.Response()


@asyncio.coroutine
def demo_handler(request):
    tpl = jenv.get_template('demo.html')
    return web.Response(body=tpl.render().encode('utf-8'))

