import asyncio
import jinja2
from aiohttp import web
import aiopg
import os

dns = "dbname=findhome_rc user=m password=CrkflybqGfhjkmYfGjcnuht1992 host=localhost port=5432"

from . import issue
from .callproxy import call_handler, demo_handler


jenv = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'))


@asyncio.coroutine
def index(request):
    tpl = jenv.get_template('index.html')
    return web.Response(body=tpl.render().encode('utf-8'))


@asyncio.coroutine
def newissue(request):
    tpl = jenv.get_template('newissue.html')
    return web.Response(body=tpl.render().encode('utf-8'))


@asyncio.coroutine
def register(request):
    pool = yield from aiopg.create_pool(dns)
    with (yield from pool.cursor()) as cur:
        yield from cur.execute("INSERT INTO devices (phone_id, phone_number) VALUES ('zzz','xxx`')")
    tpl = jenv.get_template('newissue.html')
    return web.Response(body=tpl.render().encode('utf-8'))


@asyncio.coroutine
def init(loop):
    app = web.Application(loop=loop)

    app.router.add_static('/fonts', 'public/fonts')
    app.router.add_static('/css', 'public/css')
    app.router.add_static('/js', 'public/js')

    app.router.add_route('POST', '/call', call_handler)

    app.router.add_route('GET', '/demo', demo_handler)
    app.router.add_route('GET', '/', index)
    app.router.add_route('GET', '/newissue', newissue)
    app.router.add_route('GET', '/issue/get', issue.get_data)
    app.router.add_route('POST', '/issue/set_title', issue.set_title)
    app.router.add_route('POST', '/issue/add_company', issue.add_company)
    app.router.add_route('POST', '/issue/modify_company', issue.modify_company)
    app.router.add_route('GET', '/register', register)

    srv = yield from loop.create_server(app.make_handler(),
                                        '127.0.0.1', 8080)
    print("Server started at http://127.0.0.1:8080")
    return srv


def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(init(loop))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    self.db = momoko.Pool(dsn=options.postgresql_config, size=1)

if __name__ == '__main__':
    main()
