import json
from aiohttp import web
from contextlib import contextmanager


@contextmanager
def modify():
    try:
        with open('data/data.json', 'rt') as f:
            val = json.loads(f.read())
    except FileNotFoundError:
        val = {'companies': []}
    yield val
    with open('data/data.json', 'wt') as f:
        f.write(json.dumps(val))


def set_title(req):
    json = yield from req.json()
    with modify() as data:
        data.update(json)
    return web.Response(body=b'')


def add_company(req):
    json = yield from req.json()
    with modify() as data:
        data['companies'].append(json)
    return web.Response(body=b'')


def modify_company(req):
    json = yield from req.json()
    with modify() as data:
        for c in data['companies']:
            if c['id'] == json['id']:
                c.update(json)
    return web.Response(body=b'')

def get_data(req):
    with modify() as data:
        return web.Response(body=json.dumps(data).encode('utf-8'))
